Page layout with Bootstrap
==========================

Bootstrap is the most popular front-end framework that implements RWD
(Responsive We Design). Bootstrap is bloated with many other features, which
can be considered as a flaw. We will cover only RWD and grid.

Sample project is bootstrap_demo. It is divided into steps, like Layout Demo.

Create your own master branch and checkout it:

```sh
git branch student/your_login/master
git checkout student/your_login/master
```

Preparing html
--------------

Like always start with basic HTML structure.

|step|
|:--:|
|  0 |

To display step/0:

```sh
git diff master origin/step/0
```

Reproduce those changes in your branch, add changes, commit and set version tag:

```sh
git tag student/your_login/step/0
```

Install Bootstrap
-----------------

Go to https://getbootstrap.com/ -> getting started

Copy link addres of Download Bootstrap (compiled and minified).

In console in bootstrap_demo dir (to paste in console: Shif+Insert):

```sh
wget ~paste link address here~
unzip bootstrap-*-dist.zip
rm bootstrap-*.zip
mv bootstrap-* bootstrap
```

### jQuery

Bootstrap requires jQuery. jQuery is a JavaScript library, that's all you need
to know for the moment.

Go to http://jquery.com/download/ copy link address of "Download the
compressed, production jQuery ..."

```sh
wget ~paste link address here~
mv jquery-*.min.js jquery.min.js
```

Rest is done inside index.html file.

|step|
|:--:|
|  1 |

To display step/1:

```sh
git diff origin/step/0 origin/step/1
```

In this step you will see many files from Bootstrap created. Interesting
changes are done in index.html. We can narrow `git diff` to specific files:

```sh
git diff origin/step/0 origin/step/1 -- index.html
```

First Bootstrap element
-----------------------

|step|
|:--:|
|  2 |

Look at step 2.

What you see on the page (in the browser) are two columns or two rows, it
depends on window size. If your window is wide there are two columns, if you
narrow the window columns collapse into rows. You can say that, if there is
enough space columns are columns, but if there is not enough space columns takes
what's left (whole width of narrow window) and are displayed one under another.

Container `<div class="container">` contains rows `<div class="row">`, rows
contains columns e.g. `<div class="col-xs-6>`, columns contains anything.

Hierarchy container > row > column must be preserved, no element can be in
between. Column must be direct child of row, and row must be direct child of
container. Don't confuse direct child with first child ;)

Usually there is only one container, but two or more are allowed if needed.
Some content might be outside the container.

### Column naming

Column name is "col-SS-XX", where:
* SS tells on what screen size column must collapse
  * lg - collapses if screen is less than large (1170px)
  * md - collapses if screen is less than medium (992px)
  * sm - collapses if screen is less than small (750px)
  * xs - columns for very small screen, never collapses
* XX - column width from 1 to 12. Container is divided into 12 units (in width).
  So, sum of width of all columns in row can be maximum 12. Columns can have
  different width. There can be:
  * 12 columns of width 1
  * 6 columns of width 2
  * 2 columns of width 6
  * 1 column of width 7 + 2 columns of width 2 + 1 column of width 1

Example of the last case:

```sh
git checkout origin/demo/0
```

Look at the browser and at the source code.

And go back to your branch:

```sh
git checkout student/your_login/master
```

Add some dummy content
----------------------

> Word "dummy" is often used to describe some fake data or objects that supposed
> to mimic real data or objects.

|step|
|:--:|
|  3 |

Few images ware created there, so you can limit step/3 preview to index.html and
main.css

To get images from my commit:

```sh
git checkout origin/step/3 -- images
```

Columns, more columns!
----------------------

|step|
|:--:|
|  4 |

Example with 4 columns.

Columns inside columns
----------------------

|step|
|:--:|
|  5 |

Example with nested columns. Remember that you need to put nested columns in its
own row.

Dynamic number of columns
-------------------------

|step|
|:--:|
|  6 |

If you have four `<div class="col-lg-3 col-sm-6">` in a `<div class="row">`
that's mean:
* on large (lg) screen will be four columns of size 3
* on small (sm) screen will be two columns (in to rows) of size 6
* on extra small screen all columns will collapse to rows

Navigation bar
--------------

|step|
|:--:|
|  7 |

Navigation bar form Bootstrap automatically collapse into mobile friendly menu.

Other features
--------------

Bootstrap provides many other features - https://getbootstrap.com/css/#type
