HTML intro
==========

Every document here is a markdown document. There is a quick
[Markdown intro](lessons/useful_little_things/markdown_intro.md).

Read [Git intro](/lessons/git/000_git_intro.md) if you don't know git.

Get the sample project
----------------------

There is a sample project that you will work on. Clone it directly from my repo
**https://gitlab.com/learn_to_code/food_base** or fork it and the clone it
from your own repo.

Ready? Go!
----------

Open file *food_base/index.html* (sample project) in the browser.

This is project of web portal about food. There is not much there, it's your job
to develop it. Open *food_base/index.html* in the editor.

> There is a plugin **atom-html-preview** for Atom editor, that can display
  preview of html file side by side with the source code. `Ctrl+Shift+h`
  displays the preview

HTML (Hyper Text Markup Language) document is build of **tags**. Tag is that
thing in angle brackets `<` `>`, like `<div>` - div tag. Tag can have content:

```html
<div>This is content</div>
```

Tag usually has its closing tag, the one **with the slash** e.g. `</div>`.

There can be tags inside tags.

```html
<div>This is content. <strong>This is important.</strong></div>
```

Add text to the content of page
-------------------------------

In *food_base/index.html* find `div` tag with `id` "content" (line 62), this
fragment:

```html
<div id="body" class="container">
  <div class="row">
    <div id="sidebar" class="col-md-3">
      sidebar
    </div>
    <div id="content" class="col-md-9">

    </div>
  </div>
</div>
```

Put there tag div, with closing tag: `<div></div>`.

Save the file and checkout the page, nothing happen? That's right, because the
tag is empty. Put "Hello world!" inside `<div>` tag you've just added.

> @FixMe: content of tags doesn't require quotation marks, show this by an
  example

> [About Hello World](/lessons/useful_little_things/coding-rule_book.md#hello-world)

Tag `<div>` is a general purpose tag. For longer texts - paragraphs is `<p>`
tag. After div tag you've just added add `<p>` tag with some longer text in it.
If you don't know what to write,
["Lorem ipsum"](/lessons/useful_little_things/lorem_ipsum.md) might be fine.

You should not use `<p>` inside other `<p>` like:

```html
<!-- Wrong! -->
<p>
  This is paragraph.
  <p>
    This is second paragraph.
  </p>
</p>
```

This is proper usege of `<p>` tags:

```html
<!-- Good -->
<p>
  This is paragraph.
</p>
<p>
  This is second paragraph.
</p>
```

Text between `<!--` `-->` is a comment, web browser wont display it, it is an
information for the developer.

Usually when you write some text you want to give it a heading. There is a tag
for heading - `<h1>`.

Do you remember div tag with "Hello world!", change it to heading `<h1>` tag.

Commit your code
----------------

> [When to commit?](/lessons/git/git-rule_book.md#when_to_commit?)

```sh
git add --all
git commit --message="Dummy text on the content section"
```

If you've made your own fork of the "Food Base" project you should `push` commit
to "origin" repository. For the first time give `--set-upstream` parameter. Git
will remember "master" branch of the "origin" repository as the default one for
`push` operation.

```sh
git push --set-upstream origin master
```

Next time simple `git push` will be enough.

Validate your project
---------------------

Get familiar with [validate project](/lessons/meta/validate_project.md).

Now set the version.

```sh
git tag version-0.2.0
```

Push it if you are working on forked repository.

```sh
git push --tags
```

Compare solutions.

```sh
git diff version-0.2.0 sample_version-0.2.0
```

Later on I will just point out:

|version|
|:-----:|
|  0.2  |

The initial version of Food Base project is 0.1.0.

Tags, more tags!
----------------

Look closer at header of the page, this fragment:

```html
<div id="banner">
  <div class="container">
    <div class="row">
      <div id="top-label" class="col-xs-12">
        <div id="logo">
          <div class="label">Food</div>
          <div class="hr"></div>
          <div class="label">Base</div>
        </div>
        <p id="slogan">
          Food is any substance[1] consumed to provide nutritional support
          for the body. It is usually of plant or animal origin, and
          contains essential nutrients, such as carbohydrates, fats,
          proteins, vitamins, or minerals. The substance is ingested by an
          organism and assimilated by the organism's cells to provide
          energy, maintain life, or stimulate growth.
          - https://en.wikipedia.org/wiki/Food
        </p>
      </div>
    </div>
  </div>
```

There is a quotation from Wikipedia about food. Lets make a real link at the
end of the text to the source.

Tag for link is '<a>'. Replace "https://en.wiki..." string with tag `<a>` with
"Wikipedia" content:

```html
<a>
  Wikipedia
</a>
```

It does not work yet, you need to define to what it should point to. For that
purpose use **attribute** `href`. Attributes are placed inside the opening tag.

```html
<a href="https://en.wikipedia.org/wiki/Food">
  Wikipedia
</a>
```

I've split this into three lines, for readability. It can be in one line. Both
forms are valid:

```html
<a gref="https://google.com">
  Search
</a>
```

```html
<a gref="https://google.com">Search</a>
```

Both version produces same result in the browser.

There is "[1]" in the text, make it link to
https://en.wikipedia.org/wiki/Food#cite_note-1

If you want to emphasize something important wrap it with `<strong>` tag.
Like list of foods components "carbohydrates, fats, proteins, vitamins, or
minerals"

```html
<strong>carbohydrates, fats, proteins, vitamins, or minerals</strong>
```

Make link to Wikipedia always in new line. Put `<br />` tag just before the
"-".

```html
[...]
energy, maintain life, or stimulate growth.
<br />- <a href="https://en.wikipedia.org/wiki/Food">
  Wikipedia
</a>
```

`<br />` tag does not have it's closing tag. It is **void tag** It points place
in text where should be line break. As you probably know, making new line in
source code does not make new line in the page.

You might see `<br>` or `<br/> (without space)` version. They are also valid.
Use void tag with **" />"** endings (with space) like `<br />`.

|version|
|:-----:|
|  0.3  |

Internet is for... images!
--------------------------

Lets add an image.

Back to the content section. Add `<img />` tag inside `<p>` tag with "lorem
ipsum" text, at the end of `<p>`.

```html
<img />
```

Add attribute `src` with value "images/food_mini.jpg".

```html
<img src="images/food_mini.jpg" />
```

Each image should have textual description, for those who can not see pictures
like people with disabilities, devices not capable to display images. It
also helps with positioning on Internet search engines.

To set image description set a `alt` attribute.

```html
<img src="images/food_mini.jpg" alt="dish, food" />
```

|version|
|:-----:|
|  0.4  |

Nice, but lets make this image a link to its bigger version. Warp the image with
`<a>` tag and set `href` attribute value to "images/food.jpg".

```html
<a href="images/food.jpg">
  <img src="images/food_mini.jpg" alt="dish, food" />
</a>
```

In the `href` attribute address is relative, it does not start with "http://".
It is relative to the current file, "food_base/index.html" in this
case. The image is "food_base/images/food.jpg" so you only need to give
"images/food.jpg". Same rule applies to attribute `src` in `img` tag.

Files (not all) in the project right now:

```
food_base/index.html
food_base/images/food_mini.jpg
food_base/images/food.jpg
```

|version|
|:-----:|
|  0.5  |

Navigation
----------

There is a menu on the page make it actually do something. Find code responsible
for the menu. Edit links that *Home* points to *index.html* file, *Blog* to
*blog.html*, *About* to *about.html* and *Data* to *data.html*.

The problem is, there is no such files. Make them by coping *index.html* with
console command `cp`

```sh
cp index.html blog.html
```

Do same for About and Data.

All those files are the same. Open each one and change content (the div with
`id` "content"). Put there only heading level one with appropriate text.
Example for "Blog" file.

```html
<div id="body" class="container">
  <div class="row">
    <div id="sidebar" class="col-md-3">
      sidebar
    </div>
    <div id="content" class="col-md-9">
      <h1>Blog</h1>
    </div>
  </div>
</div>
```

Now you can clik on links in menu - it should switch between pages.

|version|
|:-----:|
|  0.6  |

Summary
-------

This is the end of this lesson. You've earned new badge... just kidding I don't
give any badges - gained knowledge is your prize.

Next lesson is [CSS Intro](/lessons/html_css/001_css_intro.md)
