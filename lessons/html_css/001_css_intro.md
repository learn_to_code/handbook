CSS intro
=========

In this lesson we will work on *about.html* files and related css files.

Bugfix
------

There is a bug. There are two `meta` tags with `name="viewport"` in section
`<head>`. There should be only one.

> Web browser doesn't crash or complain because of it. Browsers tries to fix
  many encountered errors. but you shouldn't rely on this, different browsers
  fixes errors in different way. Behavior could change from version to version.

Remove one of doubled tag, add change and commit. Give it commit message
something like "meta viewport fix"

> You should always commit such fixes in separate commits for sake of clarity.

|version|
|:-----:|
|  0.7  |

> Such fixes should increase last number in the version, but in this case last
  number is for your fixes only.

More content to work on
-----------------------

We will need few pictures. Get them from my branch sample_solution/0.8

> @ToDo: student can not checkout this, sample_solution/... is not available

```sh
git checkout sample_solution/0.8 -- images/writing.jpg images/lightbulb.jpeg \
    images/p1.jpg images/p2.jpg images/p3.jpg images/p4.jpg
```

> `checkout` command can get list of files after double dash `--` then only
  those files will be checkout.

Add following section to div with id **content**.

First two paragraphs:

```html
<p>
  <img src="images/writing.jpg" alt="coffee, notebook, laptop" />
  This is portal about food. What makes as different form many other
  portals like this is that we are value the most reliability of
  information. That's way we present here only scientifically proven
  information. We always provide sources. We constantly challenge
  ourselves and revise our knowledge. We are aware that new
  discoveries brings new facts to light and sometimes obsoletes old
  ones. We are monitoring all the sources of new discoveries and we
  are providing you updates as soon as possible.
</p>
<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
  ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
  aliquip ex ea commodo consequat. Duis aute irure dolor in
  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
  pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
  culpa qui officia deserunt mollit anim id est laborum.
</p>
<p>founder: Jim Josh</p>
```

### Next section

We have new tag here `<ol>` - Ordered List. In `<ol>` you can use
only `<li>` tags - List Item. Inside '<li>' tags you can put just text or you
can use other tags as well.

```html
<h2>Principles</h2>
<img src="images/lightbulb.jpeg" alt="lightbulb" />
<p>
  We choose to follow certain rules. We believe that this is only way
  to improve our life quality, Our principles are:
</p>
<ol>
  <li>Seek for experiments and scientific research.</li>
  <li>Seek for independent verification of previous point.</li>
  <li>Never create any statement and seek for confirmation afterwords.</li>
  <li>Ask question, then seek for the answer.</li>
  <li>Accept every answer to your questions that science is providing you.</li>
  <li>
    But always be prepared to revise your opinions when new facts are
    discovered.
  </li>
  <li>Question everything (with constructive arguments)</li>
</ol>
```

### Team presentation

Here we uses `<ul>` tag, which stands for Unordered List. `<ul>` consists of
`<li>`, inside `<li>` you can put any tag, like in this case. `<ul>` by default
presents bullet list, we will redefine this behavior later.

```html
<h2>Our team:</h2>
<ul>
  <li>
    <h3>Anna Atkinson</h3>
    <img src="images/p1.jpg" alt="Anna Atkinson" />
    <p>Specialist of cooking, neutrients, vitamins, minerals.</p>
  </li>
  <li>
    <h3>Rohn Rambo</h3>
    <img src="images/p2.jpg" alt="Rohn Rambo" />
    <p>Specialist of sport diets, diet supplements, vitamins.</p>
  </li>
  <li>
    <h3>Megan Megamen</h3>
    <img src="images/p3.jpg" alt="Megan Megamen" />
    <p>Specialist of human digestive system, metabolism.</p>
  </li>
  <li>
    <h3>Edie Engel</h3>
    <img src="images/p4.jpg" alt="Edie Engel" />
    <p>Specialist of cultivation, growing optimization processes.</p>
  </li>
</ul>
```

### Table

In next section we have a table. Tag for table is `<table>` :) Lets create some
simple table.

```html
<table>
  <tr>
    <td>Adam</td><td>31</td>
  </tr>
  <tr>
    <td>Monic</td><td>21</td>
  </tr>
</table>
```

Add parameter `border="1"` to table tag. This will add borders to cells of this
table. You should not add border in this way, we are using it only because this
is temporary table. I will show you proper way later.

`<tr>` - Table Row.
`<td>` - Table Data.

Add headers.

```html
<table border="1">
  <tr>
    <th>Name</th><th>Age</th>
  </tr>
  <tr>
    <td>Adam</td><td>31</td>
  </tr>
  <tr>
    <td>Monic</td><td>21</td>
  </tr>
</table>
```

`<th>` - Table Head

OK, remove this table and put this content:

```html
<h2>Contact us</h2>
<table>
  <tr>
    <th>Department</th>
    <th>Country</th>
    <th>Address</th>
    <th>Contact</th>
  </tr>
  <tr>
    <td>Headquarter</td>
    <td>USA</td>
    <td>
      PC Street 128<br />
      1024 Silicon Valley
    </td>
    <td>
      <a href="mailto:martin@food-base.com">martin@food-base.com</a>
    </td>
  </tr>
  <tr>
    <td>NY quarter</td>
    <td>USA</td>
    <td>
      Woody Street 1<br />
      1010 New York
    </td>
    <td>
      <a href="mailto:allen@food-base.com">allen@food-base.com</a><br />
      phone: 123-12-12
    </td>
  </tr>
  <tr>
    <td>London quarter</td>
    <td>UK</td>
    <td>
      Mt. Python Street 7<br />
      100 London
    </td>
    <td>
      <a href="mailto:chris@food-base.com">chris@food-base.com</a>
    </td>
  </tr>
  <tr>
    <td>Berlin quarter</td>
    <td>Germany</td>
    <td>
      Berlin Wall Street 13<br />
      1234 Berlin
    </td>
    <td>
      <a href="mailto:andreas@food-base.com">andreas@food-base.com</a>
    </td>
  </tr>
</table>
```

### Last section

Nothing special here.

```html
<h2>Summary</h2>
<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
  ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
  aliquip ex ea commodo consequat. Duis aute irure dolor in
  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
  pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
  culpa qui officia deserunt mollit anim id est laborum.
</p>
<p>
  Food Base Team
</p>
```

|version|
|:-----:|
|  0.8  |

First styles
------------

To change appearance of HTML elements you need to use CSS styles. There are few
ways to do that.

### Style inside the tag

Let say we want to justify text of `<p>`. Try it on first paragraph in the
content.

<p style="text-align: justify;">

But what if you want justify all the paragraphs? Most of styles is inherited.
Move `style="text-align: justify;"` from `<p>` to `<div id="content">`. All
`<p>` inside "content" inherits style from its parent.

Not all elements (tags) inherits all styles, e.g. `<a>` doesn't inherit color
(it inherits other styles). Experiment on `<div id="slogan">`. Change font:

```html
<div id="slogan" style="font-family: serif;">
```

and then change color:

```html
<div id="slogan" style="font-family: serif; color: red;">
```

OK, remove style from slogan.

You can sometimes encounter to old way of changing appearance of HTML elements
with special parameters, like:

<p align="justify">

> There is `<font>` tag for changing the font parameters. Those are not CSS, you
  should never ever use them. Those are relicts of old times when there was no
  CSS.

Parameter *style* is valid CSS technique but you'd better use other ways to add
styles. Parameter style used inside html tag introduce coupling styles with
content.

Another way to add styles is to add `<style type="text/css">` tag in *head*
section, but this couples styles with specific html file...

Best way to add styles
----------------------

Best way is to link to external .css file. Put this inside `<head>` tag. Find
other links and keep them together.

```html
<link rel="stylesheet" href="content.css" charset="utf-8">
```

> BTW. `<head>` tag is invisible for the user, it contains all sort of page
  parameters and configurations.

We have link to style file, now create the file, name it *content.css*, place it
in main directory, where other css file *main.css* is.

> Remove style from `<div id="content">` we will redefine it on external file.

### Justify text

To style a tag write a definition for it. Put this in *content.css* file.

```css
p {
  text-align: justify;
}
```

You can define multiple properties, add font family definition.

```css
p {
  text-align: justify;
  font-family: serif;
}
```

This affects all `<p>` on the page, like slogan at the header. But we want to
redefine styles only inside the content - div with id "content".

```css
#content {
  text-align: justify;
  font-family: serif;
}
```

Character **#** tells that we want to define id "content". But now, we've
redefined every element inside `#content`. To define only `<p>` inside
`#content`

```css
#content p {
  text-align: justify;
  font-family: serif;
}
```

Lets change color of heading level 1 and make it bold.

```css
h1 {
  color: #6b400e;
  font-weight: bold;
}
```

Make headings level 2 and 3 same as 1. Instead of writing identical definitions
for each, stack many selectors on one definition. Pay attention to comas.

```css
h1, h2, h3 {
  color: #6b400e;
  font-weight: bold;
}
```

|version|
|:-----:|
|  0.9  |

Style images
------------

```css
img {
  border-style: solid;
  border-width: 3mm;
  border-color: #666252;
}
```

And narrow it to `#content`

```css
#content img {
  border-style: solid;
  border-width: 3mm;
  border-color: #666252;
}
```

There is compact form of border definition:

```css
#content img {
  border: solid 3mm #666252;
}
```

> Compact form is equally good as regular one. Use the one you find more
  suitable for each situation.

Signatures
----------

We have two paragraphs that are some sort of signatures. One at the end of first
section *founder: Jim Josh*, second at the end of content *Food Base Team*.

When you have few elements that you want to style use class. Add class to the
elements:

```html
<p class="signature">founder: Jim Josh</p>
[...]
<p class="signature">
  Food Base Team
</p>
```

And define the class

```css
.signature {
  text-align: right;
  font-size: 1.5em;
  font-style: italic;
  margin-top: 2em;
  margin-bottom: 2em;
  margin-right: 10mm;
}
```

We have new units here
* **em** - 1em is size of current font (relative unit)
* **mm** - millimeter (absolute unit)

`font-size: 1.5em;` means that font size will be increased by 50% from current
size (whatever it is now). This unit is relative to font size of parent
element. `margin-right: 10mm;` millimeters are real life millimeters if you
have standard monitor with 96ppi or if you have non-standard monitor with
correctly configured screen ppi in operating system. You should use units like
cm, mm, in with caution, some systems interpreter them incorrectly. Don't use
this units where it can brake page layout.

But there is one problem, `text-align: right;` doesn't apply - signature is
still aligned to left. It is that because definition `#content p` covers
`.signature`. Selector `#content p` is more detailed than selector `.signature`.

Note that only conflicting rule `text-align` does not apply. Others works fine.

So, we need make our selector more detailed than `#content p`:

```css
#content .signature {
  text-align: right;
  font-size: 1.5em;
  font-style: italic;
  margin-top: 2em;
  margin-bottom: 2em;
  margin-right: 10mm;
}
```

> Why this is more detailed? You could say that `.signature` is on same level as
  `p`. But class has higher priority than tag, (id has higher priority than
  class) tag < class < id

|version|
|:-----:|
| 0.10  |

Make images float
-----------------

We could make this:

```css
#content img {
  float: left;
}
```

But we want two kind of images, one that floats left and one that floats right.
To achieve that we need assign classes to our images.

```html
<img src="images/writing.jpg"
     class="left"
     alt="coffee, notebook, laptop" />
...
<img src="images/lightbulb.jpeg" class="right" alt="lightbulb" />
```

> Tag can be split into lines, if it is to long. When you decide to split it,
  make each parameter in new line.

Define our styles:

```css
.left {
  float: left;
  margin-right: 3mm;
  margin-bottom: 3mm;
}

.right {
  float: right;
  margin-left: 3mm;
  margin-right: 0px;
}
```

It works, but good practice is to limit those classes to specific tag. Our
intention is to style images and nothing else.

```css
img.left {
  float: left;
  margin-right: 3mm;
  margin-bottom: 3mm;
}

img.right {
  float: right;
  margin-left: 3mm;
  margin-right: 0px;
}
```

Note that `img .right` (with space between) would be something different than
`img.right` (without space between). Selector `img .right` selects elements
with class *right*, that are inside *img*. (in this case such rule is
pointless, img can't have any element inside). Selector `img.right` selects all
`<img>` tags that also have class *right*.

Members list
------------

Members list is typical list with bullet points. But we want all boxes to float
and fill all available space. Set class *members* to '<ul>' element on the page.

Lets clear out bullets:

```css
.members {
  list-style-type: none;
  padding-left: 0;
}
```

Make it float:

```css
.members li {
  float: left;
}
```

Now, heading "Contact us" is misbehaving. Try different width of browser window.
In some cases `<h2>` goes next to (right of) person info. This is because `<li>`
have `float: left;` property. That means regular text will try to lay around of
it. Like in case of first picture. There this effect is what we want. Here we
don't want this effect, but we can't remove `float` from `<li>` - we need member
information boxes float around each other.

But we can tell '<h2>' to not flow around of floating elements.

```css
h2 {
  clear: left;
}
```

And put some space before `<h2>`

```css
h2 {
  clear: left;
  padding-top: 1em;
}
```

> We use `padding-top` (inside margin) instead of `margin-top` because regular
> margin doesn't apply to floating elements. You can try it.

It's not perfect yet. Members boxes have different sizes - it looks bad. For
clear out what exactly is going on add `border` to items, temporarily.

```css
.members li {
  ...
  border: solid 1px red;
}
```

Make items equal size.

```css
.members li {
  ...
  width: 270px;
  height: 320px;
}
```

In this case we need use pixels because our layout depends on it.

Define more properties, add one at the time and observe its effect.

```css
.members li {
  ...
  text-align: center;
  background-color: #e3e0d8;
  margin: 10px;
  padding: 10px;
  border-radius: 1mm;
  box-shadow: 0mm 0mm 1mm #9a988e;
}
```

To see the effect, comment out border, in atom use Ctrl+/ to toggle comment on
current line or whole selection.

```css
.members li {
  ...
  /*border: solid 1px red;*/
  ...
}
```

|version|
|:-----:|
|  0.11 |

Special effects
---------------

We need to put content of each `<li>` into `<div>` with class *box*, like this:

```html
<ul class="members">
  <li>
    <div class="box">
      <h3>Anna Atkinson</h3>
      <img src="images/p1.jpg" alt="Anna Atkinson" />
      <p>Specialist of cooking, neutrients, vitamins, minerals.</p>
    </div>
  </li>
```

Do it for all items.

Then define new CSS rule `.members .box`. You need to move some properties from
`.members li` to `.members .box`. Properties that you need to move to new
location:

```css
.members .box {
  text-align: center;
  background-color: #e3e0d8;
  margin: 10px;
  padding: 10px;
  border-radius: 1mm;
  box-shadow: 0mm 0mm 1mm #9a988e;
}
```

This done nothing special, but we can add some interesting rules to `.box`.

```css
.members .box:hover {
  box-shadow: 1mm 1mm 3mm #9a988e;
}
```

Now, when you move mouse cursor over the member shadow will change. `:hover`
is a pseudo selector that selects elements when mouse cursor is above the
element. Add some more properties to improve hover effect:

```css
.members .box:hover {
  box-shadow: 1mm 1mm 3mm #9a988e;
  margin-left: 8px;
  margin-top: 8px;
  margin-right: 12px;
  margin-bottom: 12px;
}
```

And few more general improvements:

```css
.members p {
  text-align: inherit;
  margin-top: 5px;
}

.members h3 {
  margin-top: 7px;
}

.members img {
  border-radius: 3px;
  border-color: #666560;
  float: none;
}
```

And adjust (change) height of `.members li` element:

```css
.members li {
  ...
  height: 340px;
}
```

|version|
|:-----:|
|  0.12 |

Table
-----

Next we have table with contact information, lets prettify it.

Add border to cells:

```css
td, th {
  border: solid 1px black;
}
```

> Yes, you can give color as a word, basic recognizable names are listed here
> https://www.w3.org/TR/css3-color/#html4

Set background color of table head:

```css
th {
  background-color: #918b7f;
}
```

Add space inside the cell:

```css
td, th {
  ...
  padding-top: 0;
  padding-right: 4mm;
  padding-bottom: 0;
  padding-left: 4mm;
}
```

which can be written in short form:

```css
td, th {
  ...
  padding: 0 4mm 0 4mm;
}
```

Values in order are top, right, bottom, left. It's clock wise order starting
from 12.

It can be shorten even more:

```css
td, th {
  ...
  padding: 0 4mm;
}
```

In that form first value are top/bottom, left/right

All properties that ends on "-top", "-right", "-bottom", "-left" can be written
in this forms.

Add padding to table head:

```css
th {
  padding-top: 2mm;
  padding-bottom: 2mm;
}
```

We just redefined top and bottom padding of `<th>` that we previously defined in
`td, th` rule. It is a valid technique in CSS. You can redefine properties.
But use this technique with caution, it might be very confusing.

Change background color of every second element.

```css
tr:nth-child(odd) {
  background-color: #bbb8ad;
}
```

`:nth-child()` is a pseudo selector that selects elements by counting them as
children of parent element. In this example it selects every odd `tr`, parent
element is `table`.

As an argument for 'nth-child()' you can pass:
* odd - every odd elements
* even - every even elements
* X - where X is a number, it will select X-th element, 1 - first, 2 - second...
* formula - eg. 2n, 2n+1, 3n, where n is a counter of elements

Address problem
---------------

Our table can change its size automatically, when parent element,
`<div id="content">` in this case, is to small to fit whole table. Table will
squeeze its content to fit in. But then address can split into too many lines.
To prevent that set class "address" to each `<td>` that contains the address:

```html
<td class="address">
  PC Street 128<br />
  1024 Silicon Valley
</td>
```

And define CSS rule for it:

```ccs
.address {
  white-space: nowrap;
}
```

This prevents from automatic splitting text into lines. New line is only where
it is specified to be, eg. each '<p>' starts from new line or '<br />' sets new
line.

|version|
|:-----:|
|  0.13 |
