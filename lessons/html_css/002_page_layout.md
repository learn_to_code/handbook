Page layout
===========

In this lesson we will work on sample project
[layout_demo](https://gitlab.com/learn_to_code/layout_demo). Clone it.


### Instruction

Layout Demo is divided into steps (not in versions). Steps are numbered with
single numbers - step/1, step/2, step/3...

Create own master branch:

```sh
git branch student/your_login/master
git checkout student/your_login/master
```

Develop, add changes, commit and set your step number:

```sh
git tag student/your_login/step/1
```

Compare your step to mine:

```
git diff student/your_login/step/1 origin/step/1
```

Display what has been changed in step 2:

```sh
git diff origin/step/1 origin/step/2
```

HTML init
---------

Starting point of each HTML document.

*index.html*:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Layout Demo</title>
  </head>
  <body>
    Hello World!
  </body>
</html>
```

* `<!DOCTYPE html>` - This tells that this page is written in HTML version 5.
  Use this as a default option.
* `<html>` - Everything should be inside this tag.
* `<head>` - Place for parameters of the page and links. Stuff here is
  not visible to the user.
  * `<meta charset="utf-8">` - Charset encoding. Always use **utf-8**, even if
    the page is only in English. Basically charset defines how to manage
    international characters like 'Ł'. Default ASCII covers only English
    alphabet. UTF-8 covers all languages ever known to human race and more like
    symbols, icons, arrows.
  * `<title>` - It is title on title bar of browser window.
* `<body>` - Visible part of the page.

|step|
|:--:|
|  0 |

CSS Link
--------

*index.html*:

```html
...
<head>
  ...
  <link rel="stylesheet" href="main.css" />
</head>
...
```

*main.css*:

```css
body {
  background-color: #eeeeee;
}
```

CSS Reset
---------

Different browsers have different default styles. It is good practice to reset
all important settings, so that the page looks the same on every browser.

This is very simple version of CSS reset. Put this at the beginning of the file.
That you can redefine those properties later on. If you put this in the middle
of the file or at the end, you will possibly reset your own rules.

*main.css*:

```css
* {
  margin: 0;
  padding: 0;
  border: 0;
}
```

`*` - selector that selects all tags, it doesn't work on IE6, at the time of
writing this document IE6 usage is 0.06%, IE7, IE8, IE9 also practically doesn't
exists. I'm writing about this, because Internet Explorer until version 10 was
crapware and web developers has to apply many different hacks to the page that
it looks good on IE. You will encounter those hacks in older materials.

Extended version of CSS reset is here http://meyerweb.com/eric/tools/css/reset/

|step|
|:--:|
|  1 |

Block, inline and inline-block
------------------------------

> @ToDo: Explain what inline is...

Static width
------------

I will demonstrate page with static width. It is simpler to develop, but page
that dynamically fits in window is more usable for the user. We need to start
from something simple ;)

I set page width to 960px, it is good value for page width, it fits into small
screen with 1024 pixels width. Laptop and desktop screens are much bigger than
that, but there are plenty of tablets and smart devices with small screen.

To lay page on the center (in x axis) use `margin: 0 auto;`

|step|
|:--:|
|  2 |

To see changes made from step/1 to step/2

```sh
git diff origin/step/1 origin/step/2
```

Reproduce those changes in your version.

Header, Content, Footer
-----------------------

I divided page into three section header, content and footer.

|step|
|:--:|
|  3 |

Display changes made in step 3

```sh
git diff origin/step/2 origin/step/3
```

Reproduce it.

Side bar
--------

Side bar is a div with property float set to left, it could be right also.
If you set float of an element to left or right, two things happen:
* Width of the element is as small as possible unless it is defined explicit.
* Non-floating content will fill space left by floating elements, if there is no
  space left, non-floating content will be under floating elements.

It is good practice to put article into its own div and give it an id (like
"article") even if you don't define any CSS rules for it.

|step|
|:--:|
|  4 |

Display and reproduce changes made in this step ;)

Columns
-------

Article is divided on two columns. All divs are floating now, although one of
columns could still be non-floating it is more consistent like this.

|step|
|:--:|
|  5 |

Menu
----

Menu is `<ul>` with class "menu", its items must flow to left. `<ul>` itself
also floats to left because it contains only float elements. Without floating
its height would be 0.

Each menu item contains label and submenu. Submenus don't float because they are
vertical menus. Submenu have `position: absolute;` - it does not change
position or size of floating nor block elements. Absolute elements are like
above floating and block elements.

The default position is *relative*.

Submenu have also `display: none;` - it is not displayed. Sub menu is displayed
when user puts mouse cursor above corresponding menu item -
`.menu li:hover .submenu`

|step|
|:--:|
|  6 |

Dynamic width
-------------

This page can be converted into page that automatically fits into window.
Presented solution isn't perfect, when you resize window to small width content
in columns is squeezed to much. If you put article into one column (no column
division) page would be OK.

|step|
|:--:|
|  7 |

Responsive Web Design
---------------------

There is a design pattern that is intended to create page that looks good on
all size screens, from small smartphones to large PC screens. This pattern is
Responsive Web Design (RWD). It adapts page content to screen width. It changes
images size, it toggles menus between full and drop-down, changes number of
columns.

Since mobile traffic is more than half (in web traffic) it is important to
adjust page to mobile devices. You can implement RWD by yourself, or use one of
Open Source frameworks like [Bootstrap](https://getbootstrap.com/).

> Bootstrap is a front-end framework that implements few patterns and features:
> * RWD
> * grid - it divides page into columns
> * web components - many visual elements
> * font icons - it have external font, that contains graphic icons instead of
>   letters.
> * expensive themes

Here you have lesson in which you use Bootstrap to create responsive design
(/handbook/lessons/html_css/003_page_layout_with_bootstrap.md)
