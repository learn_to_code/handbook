Coding - Rule Book
==================

In random order

Max length of line
------------------

Max length of line is **80** chars.
* Longer line is probably to complex. Though it is possible to write line of
  code shorter than 80 chars and still too complex, there is no reason to write
  longer lines.
* You will often analyze side-by-side diffs of code - it's much more readable if
  each side of code fits its side.
* You will want to work in split screen mode, if your lines are 80 chars long
  you will be able display more files side by side.
* 80 chars is enough to write any logic you want, for sake of readability line
  should be as short as possible.

Hello World
-----------

You should use this sentence "Hello world!" as your first words used in any new
language you are learning. It's an old developers tradition.
