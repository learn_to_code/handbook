Markdown intro
==============

Markdown is a format of text document. Extension **.md**. It is simple text
file, use any text editor to open it and edit it. It can be converted to HTML
format with `markdown` program. There is plug-in for *Atom* which displays HTML
version of opened markdown document.

Main purpose of markdown is that you can create markdown document with basic
text editor, you can read it with basic text editor and also you can generate
well formated HTML document.

Examples:

Preview the HTML version together with text version.

First level title
=================

Paragraph of text, paragraph of text, paragraph of text, paragraph of text
paragraph of text, paragraph of text, paragraph of text.

Second paragraph of text, second paragraph of text, second paragraph of text.

Second level title
------------------

* unordered list
* unordered list
* unordered list


1. ordered list
2. ordered list
2. ordered list

### Third level title

[Link to wikipedia](http://wikipedia.org)

> This is blockquote, I'm using it to comment things.

---

#### Forth level title

This are basic features. Google for more if you are curious.
