Workflow
========

Basically this handbook is meant to be a support material for mentors and
teachers. Student reads the handbook and mentor should provide detailed
explanations when required. It is possible to learn without mentor - Internet
resources are infinite you can use them tutorials and forums (not tested in
practice yet).

Content of this handbook covers only basic concepts of each subject.
It presents you an idea behind each technology, you should find details in the
documentation provided for every technology presented here. Ask question to your
mentor or community of specific technology.

Request access
--------------

Request access to "Learn to Code" repository, in gitlab you have to get
"developer" rights to be able to push your own branch.

Clone handbook
--------------

Create "leran_to_code" dir in your projects dir. I have "projects" dir in my
home dir, I put every project there. Go to "learn_to_code" dir and clone
handbook project there:

```sh
git clone git@gitlab.com:learn_to_code/handbook.git
```

While you read the handbook you will encounter reference to projects to work on
while you learn. Then clone project repo e.g. *food_base* (go to
"learn_to_code" dir to do it)

```sh
git clone git@gitlab.com:learn_to_code/food_base.git
```

Go to food_base dir, make your own branch and checkout it (use your login from
gitlab):

```sh
git branch student/your-login
git checkout student/your-login
```

You are ready to go
-------------------

Make changes, commit it, set version tag, push it. Remember to work on your own
barnach 'student/your-login'

Mentor will validate it via gitlab comments, mentor will request changes if
necessary.

More about validating (Validate Project)[/lessons/meta/validate_project.md]
