Working with a project
======================

> @ToDo: Description...

Create Merge Request
--------------------

Checkout *develop* branch.

```sh
git checkout develop
```

Create feature branch from *develop* named this way: *feature/123-feature_name*
Where *123* is an issue number. *feature_name* is a short but descriptive name
of the feature, use "_" instead of spaces.

```sh
git branch feature/123-feature_name
```

Do your development.

Push it to the origin.

```sh
git push --set-upstream origin feature/123-feature_name
```

Go to the project page on gitlab. At the top of the page you should see

>  You pushed to feature/123-test about a minute ago  [Create merge request]

click button "Create merge request"

This top message contain only last pushed branch, if your isn't visible there
go to *Branches*, find position with yours and click "Merge Request".

Check "Remove source branch when merge request is accepted." option. Check
"Squash commits when merge request is accepted." option. Make sure that
*Target branch* is *develop*.

You can assign someone to the Merge request as an reviewer.

In the description you can put some details about your implementation, decision
you've made or point out thing on what special attention is needed.

When ready click "Create merge request"

Processing Merge request
------------------------

> @ToDo: :)
