Validate project
================

> Outdated method!!! @ToDo: Update!

There is no simple way to validate your project. Almost every task can be
accomplished in many ways. I've provided you my example solutions. You can
compare your changes with mine. Remember that differences not necessary
means your failure or worse solution. Sometimes I make simplified solution
or more descriptive than it should be for learning purpose.

My solution is in branch "sample_solution/X.Y", where X.Y is a version
number e.g. 1.0. On each step I will tell you what version you should set and
you can compare with my sample solution.

Workflow scenario
-----------------

### Start

Right after the cloning of sample project you are on branch *master*. You should
work on your own branch. Create your own master branch and checkout it:

```sh
git branch student/your_login/master
git checkout student/your_login/master
```

### Work

Scenario:

Handbook says that current state of the application is version 0.2.
Set same version of your project (you already committed your code and made a
push).

Naming convention:

You should name your version like this: "student/your_login/ver-X.Y"
where:
* your_login is your login in gitlab.com service
* X.Y is version number

Version is a tag, tag is a label that points to specific commit. Tags can be
used for many other purposes than giving a version to your software.

> Term version can be used in two meanings. In git 'version' means a specific
> revision/commit. In general 'version' means version of release, e.g. Google
> Chrome version 39 - this is software release.

```sh
git tag student/yur_login/ver-0.2
```

Push tag into "origin" repository.

```sh
git push --tags
```

Compare your version 0.2 to mine.

```sh
git diff student/your_login/ver-0.2 origin/sample_solution/0.2
```

If you need to make any changes or fixes to that version, commit them and tag
as student/your_login/ver-0.2.1, student/your_login/ver-0.2.2 and so on. Then
compare fixed version

```sh
git diff student/your_login/ver-0.2.1 origin/sample_solution/0.2
```

Tips and Tricks
---------------

You can compare your current state with sample solution.

```sh
git diff origin/sample_solution/0.2
```

Check what changed between versions.

```sh
git diff student/your_login/ver-0.2 student/your_login/ver-0.3
```

Do you want to see whole project in "sample_solution/0.2"? You have to commit
your current changes if there are any and then:

```sh
git checkout origin/sample_solution/0.2
```

and go back to your branch (your branch is probably
*student/your_login/master*).

```sh
git checkout student/your_login/master
```

Or just display specific file (e.g. index.html) in "sample_solution/0.2.0"?

```sh
git show origin/sample_solution/0.2:index.html
```

Or save it to a file?

```sh
git show origin/sample_solution/0.2:index.html > ~/index.html
```

Work on previous version
------------------------

If you want to work again on some old version, create new branch from this
version - you can pass a tag as a revision, Before you start make sure you have
your current changes committed.

```sh
git branch student/your_login/fix-ver-0.2 student/your_login/ver-0.2
```

*fix-ver-0.2* is only an example, you can set your onw name, more suitable one.

This will create new branch *student/you_login/fix-ver-0.2* from tag
*student/your_login/ver-0.2*. Checkout it

```sh
git checkout student/your_login/fix-ver-0.2
```
