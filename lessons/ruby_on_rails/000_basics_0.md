Ruby on Rails
=============

> "Rails" or "RoR" is a short from "Ruby on Rails"

Rails is a framework for building web applications. Every web application serves
some pages, takes some data from the user, almost all web applications use
relational database like MySQL. Very popular concept of managing data is to
translate records to objects. All of this and more is provided by Rails, so
you can concentrate on creating unique functionality of your application while
common features are given to you for free.

Install Rails
-------------

```sh
gem install rails
```

install `mini_racer` gem, it is needed by rails (it's not installed
automatically, because there are alternatives for it)

```sh
gem install mini_racer
```

Now we have Rails in the system. Rails is like a library, which we can build in
our own application.

Create an app
-------------

Lets create our first application. We will create an app for people who want to
post a micro job offer or for people who want to do a micro job. It is a shared
economy network for small jobs.

Go to your project directory and run command:

```sh
rails new microjob
```

Go to microjob dir. You can see there directories:

* app - there will be your core functionality
* config - there are configurations file of your app
* log - logs of your app
* public - public content, like pictures
* test - there are test for your app
* other file and dirs :)

We need to cover one thing manually:

Open *Gemfile* file, find line: `# gem 'therubyracer', platforms: :ruby` and
replace it with:

```ruby
gem 'mini_racer'
```

If you are in your app dir, you can run it:

```sh
rails server
```

You should see something like this:

```
=> Booting Puma
=> Rails 5.1.2 application starting in development on http://localhost:3000
=> Run `rails server -h` for more startup options
Puma starting in single mode...
* Version 3.9.1 (ruby 2.4.1-p111), codename: Private Caller
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://0.0.0.0:3000
Use Ctrl-C to stop
```

It provides information about what is running and where. You can see
`http://localhost:3000` it is address of your application. `localhost` means
your computer, `:3000` it is a port on what your app is listening.

> Use Ctrl-C to stop

Open the app
------------

Open you web browser and go to http://localhost:3000

You should see a greeting page, (Rails in version 5 shows "Yay! You’re on
Rails!" information).

In the console you can see the log, something like this:

```
Started GET "/" for 169.254.241.4 at 2017-07-08 22:31:42 +0200
Cannot render console from 169.254.241.4! Allowed networks: 127.0.0.1, ::1, 127.0.0.0/127.255.255.255
Processing by Rails::WelcomeController#index as HTML
  Rendering /home/mk/.gem/ruby/2.4.0/gems/railties-5.1.2/lib/rails/templates/rails/welcome/index.html.erb
  Rendered /home/mk/.gem/ruby/2.4.0/gems/railties-5.1.2/lib/rails/templates/rails/welcome/index.html.erb (3.5ms)
Completed 200 OK in 166ms (Views: 7.9ms)

```

HTTP
----



First page
----------

