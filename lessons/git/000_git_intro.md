Git intro
=========

Git is a version control system. It tracks changes of your code or any text.
You make changes and then you commit them into repository. Commit is a state
of code from specific point in time. Commit is like save in computer game
(https://en.wikipedia.org/wiki/Saved_game), you can always load any save and
start playing from point in time when the save was made. Similarly you can
always checkout any commit and continue from point in time when the commit was
made.

> There are many graphical user interface (GUI) tools for git, but I recommend
  you to use command line interface (CLI). Every examples here are console
  commands (CLI).

Exercise on test repository
---------------------------

Login to gitlab.com or githab.com and create empty repository e.g. *test_repo*.
Copy url of the reop. In your console:

```sh
git clone ~repository_url~
```

You have it on your computer now. It is your repository now, it's independent
repository from the origin one, though they are identical right after clone.
You can push your changes to the origin as well as to any repository if you
are allowed to (member of a group).

*Origin* is set as an upstream repository by `clone` operation. Repository
marked as *upstream* is the default for `push` and `pull` operations.

Try this (you have to be in the repo dir):

```sh
git remote show
git remote show origin -n
```

`clone` operation automatically setups remote repository *origin* to the one you
are cloning.

First commit, first push
------------------------

```sh
git status
```

Create file *git_play.txt*, put "Hello world!" text in it.

```sh
git status
```

*git_play.txt* is untracked, it is not in the repo though it is in your repo
dir. You have to explicit add it to the repo:

```sh
git add git_play.txt
git status
```

Now *git_play.txt* is prepared to be committed to the repo (it is still
not in the repo). It is called that *git_play.txt* is staged.

```sh
git commit --message="git_play.txt created."
```

From now on current state is saved forever. You can always get back to this
point in time.

```sh
git status
```

> It will say that "Your branch is based on 'origin/master', but the upstream
  is gone." it's ok, this message appears only before first push.

You can transfer changes - commits from your local repository to other
repository (e.g. remote repository). The operation is called **push**.

```sh
git push
```

You should see changes on your git service (gitlab.com *commits* section or
*files* to see your new file).

Changes are coming...
---------------------

Make some more changes to *git_play.txt*. `add` changes, `commit`.

`git status` will show info that "Your branch is ahead of 'origin/master' by 1
commit.". You can push last commit to origin repo.

Everything is in the log
------------------------

```sh
git log
```

press *q* to quit.

Every log entry looks like this:

```
commit abcd123...
Author: Mariusz Kowalski <mk@devpad.me>
Date:   Mon Oct 31 00:29:48 2016 +0100

    git_play.txt created.
```

On the first line there is commit id. You can `checkout` it. Find commit id of
commit that creates git_play.txt and `checkout` it

```sh
git checkout abcd12
```

> Yes, you can put only few first characters. It will work as long as it will be
  unique, so give at least 6 characters.

Look at the file, there is no last changes. `status` will say that you are
*HEAD detached*, that means you are in the middle of time line of your branch.

To go back to most recent revision, `checkout` the branch (*master* in this
example).

```sh
git checkout master
```

> Branching will be covered later. For now you need to know that branch is a
  different version (like copy) of your repository. The default one is *master*.

Remove, add, commit and push
----------------------------

Delete git_play.txt file. You need to add the change to the repo. Right now
`status` will tell you that git_play.txt is deleted but that change is not
staged. Add the deletion to the repo:

```sh
git rm git_play.txt
```

Now `status` also will tell you that git_play.txt is deleted, but this time the
change is staged. You are ready do `commit`. In fact, you can use `git rm` to
delete the file from disk and add this change to the repo at once.

You can add all changes at once, it's useful when you change many files.

```sh
git add --all
```

Then `commit` and `push` as usual.

Restore deleted file
--------------------

To restore removed file you need to find revision number at what the file
was in the repo by command `git log`. Then use `git show` command.

To just show the file:

```
git show 9224443f2fa:git_play.txt
```

To save the file on the disk:

```
git show 9224443f2fa:git_play.txt > git_play.txt
```

You can read content of the file at any revision you want.
