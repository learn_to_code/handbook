Git - Rule Book
===============

When to commit?
---------------

Every time you finish a task you should commit your changes to repository. By
the task I mean every small portion of code that changes something in the
project and you can't divide it on smaller portions that make sense. Often new
feature that you implements will consist of few tasks, like "database
migration", "tests for the feature", "the feature itself".

You are dividing your work into logical parts to ease code reviewer's work.
Imagine that you are telling the story of your work to the reviewer. Commit
message must provide clear information. Yes it's hard on the beginning.
