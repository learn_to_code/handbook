OOP (Object Oriented Programming)
=================================

<!-- @ToDo:
  * [ ] Class methods (static methods)
  * [ ] (Class) Constants
  * [ ] Modules
-->

First contact
-------------

### What object is?

Object is like little program inside your program. It has own methods and
variables. Program can use many objects.

### Where does an object come form?

Object is defined by programmer. There are many objects defined by language
creators. Set of object definitions that are ready to use is called standard
library (stdlib) every language has some standard library.

Object definition is called *class*. Example:

```ruby
class Visitor
  attr_accessor :first_name, :last_name
end
```

> Class name must begin with capital letter.

Now you can create an object of class `Visitor`. `new` is a method of every
class that creates an object and returns it. You can assign it to a variable.

```ruby
joe = Visitor.new
```

`joe` is an object of class  `Visitor`. We've defined two attributes
`first_name` and `last_name`. Attribute is like variable inside the object.
You can set attribute value and you can get attribute value. So attribute is
accessible from outside - you can use it outside your class, from main program.
Things that can be called outside the class are *public*, things that are
accessible only from inside the class are *private*.

> Attributes, and other public resources can also be accessed from inside the
> class.

```ruby
joe.first_name = 'Joe'
joe.last_name = 'Smith'

puts joe.first_name
puts joe.last_name
```

Objects method
--------------

If object is a little program then we can define a method for it. Lets define
method that returns full name of an visitor.

```ruby
class Visitor
  attr_accessor :first_name, :last_name

  def full_name
	return "#{first_name} #{last_name}"
  end
end
```

Out method calls object attributes, that's OK, attributes are accessible outside
and inside, inside the class to be exact.
Use it:

```ruby
joe = Visitor.new
joe.first_name = 'Joe'
joe.last_name = 'Smith'

puts joe.full_name
```

Stdlib classes
--------------

I mentioned that every (most of then) language have a standard library, that
consists of many predefined classes, classes are defined of course only in
object oriented languages.

Technically Ruby has two kind of libraries, first one is *core* - classes from
this lib are always available, second if *stdlib* - you have to tell Ruby that
you want to use a class from this library.

E.g. core class `Time`:

```ruby
time1 = Time.new
# wait some time
time2 = Time.new

puts time2 - time1
```

We've created two objects of class `Time`. `Time` object keeps the time, in this
case time of creation of the object.

Lets use some class from stdlib. Imagine you need collection of values but
without duplicates, there is `Set` class for that purpose. E.g. tag list for an
article.

```ruby
require 'set'
tags = Set.new
tags << "news"
tags << "since"
tags << "news"

puts tags.to_a.to_s
```

We've added "news" two times, but it is only once in the set.

> `to_a.to_s` converts Set to Array and then to String

`require 'set'` tells Ruby that we need it, Ruby loads this class.

Ruby has plenty of classes predefined. Check this out:
* http://ruby-doc.org/core
* http://ruby-doc.org/stdlib

First OOP program
-----------------

We will write program for reception of headquarter of some imaginary company.
Reception gives badges to visitors.

Example program is at https://gitlab.com/learn_to_code/reception

This is master branch, we will start this. This is concept version made in
procedural style.

```ruby
#!/usr/bin/env ruby

visitors = []

def user_interaction
  puts 'Enter persons number to remove'
  puts 'or enter name to add new'
  puts 'or enter "q" to quit'
  response = STDIN.gets.chomp
  if response =~ /^\d+$/
    return response.to_i
  else
    return response
  end
end

def print_state(state)
  puts
  puts 'Currently inside:'
  state.each_with_index do |person, index|
    puts "#{index} - #{person}"
  end
end

continue = true
while continue
  print_state visitors
  response = user_interaction
  if response == 'q'
    continue = false
  elsif response.is_a? Fixnum
    visitors.delete_at response
  else
    visitors << response
  end
end
```

It is fine like it is, but we want to extend its functionality. Object Oriented
Program will be much easier to develop. Change it to OOP.

Add class `Visitor`. Add it at the top.

```ruby
class Visitor
  attr_accessor :first_name, :last_name
end
```

And modify adding visitor to the list.

```ruby
while continue
  print_state visitors
  response = user_interaction
  if response == 'q'
    continue = false
  elsif response.is_a? Fixnum
    visitors.delete_at response
  else
    visitor = Visitor.new
    names = response.split(' ')
	visitor.first_name = names.first
	visitor.last_name = names.last
    visitors << visitor
  end
end
```


|version|
|:-----:|
|  1.0  |

As always you can check my sample solution:

```sh
git diff origin/master origin/sample/1.0
```

Name printing is far from perfect. We should add method `full_name` and use it.

```ruby
class Visitor
  # ...
  def full_name
    return "#{first_name} #{last_name}"
  end
end
```

Then you could use it like this:

```ruby
state.each_with_index do |person, index|
  puts "#{index} - #{person.full_name}"
end
```

But imagine that you have plenty of such cases... a lot of work. And if you are
not using the default behavior anywhere - you are not making use of printing
"#<Visitor:0x00000001f4f410>" you can change default behavior.

```ruby
puts "person: #{person}"
```

This actually calls method `to_s` (to string) of object `person`, so we can
redefine it. You could do this

```ruby
class Visitor
  # ...

  def to_s
	 return full_name
  end
end
```

But since we just call another method, we can make an alias:

```ruby
class Visitor
  # ...

  alias to_s full_name
```

`to_s` is an alias of `full_name`. Alias is like alternative name of a method.

> Method `to_s` is a method build in the language, many developers depend on
> default behavior of things. So think twice before you change any default
> behavior. In this case, it's not very risky, redefining of method `to_s` of
> your own class is OK, but redefining someones else method is most probably a
> bad idea.

|version|
|:-----:|
|  1.1  |

class in the file
-----------------

A god practice is that, you should keep each class in separate file. All class
files should be in `lib` directory (unless you are  using framework with
different directory layout, like Rails).

Ruby requires that class name begins with capital letter, technically rest of
the name can be mix of any case and char "_". But convention is that you should
use write each word from with capital letter and don't use "_" just glue words
together.

|     good       |   bad     |
|----------------|-----------|
| Car            | CAR       |
| SportCar       | CAr       |
| SuperSportCar  | Sport_car |
|                | Sport_Car |

And class file name should be same as class itself, but all lower cases and
words bonded with "_".

|  class name   |   file name        |
|---------------|--------------------|
| Car           | car.rb             |
| SportCar      | sport_car.rb       |
| SuperSportCar | super_sport_car.rb |

That way everyone knows what class is in every file and in what file the class
is.

So, move class Visitor to file lib/visitor.rb. But Ruby interpreter have to
know that you want to use this file/class.

In place where was the class `require` it. `require` looks for external
libraries. Our file is local to the project, use `require_relative`:

```ruby
require_relative 'lib/visitor.rb'
```

|version|
|:-----:|
|  1.2  |

Refactoring time
----------------

Refactoring is when you are changing some code but the functionality stays
exactly the same. Refactoring is not bug fixing. If there are bugs before
refactoring they are still there after. So what is the purpose?

* Code cleaning. Sometimes you do refactoring before development, because
  spending time on improving code quality saves you more time in development.
* Optimizing. When you want your app to work exactly the same but faster.

Look closer at object creating:

```ruby
visitor = Visitor.new
names = response.split(' ')
visitor.first_name = names.first
visitor.last_name = names.last
```

You can imagine that this situation will repeat every time you create visitor.
If you have something to do each time you create an object you put this into
`initialize` method. The object call this method right after it's created.
In other languages similar method is called "constructor", sometimes those two
names are used interchangeably.

In the class:

```ruby
class Visitor
  attr_accessor :first_name, :last_name

  def initialize(full_name)
    names = full_name.split(' ')
    @first_name = names.first
    @last_name = names.last
  end

  # ...
end
```

What is that `@first_name`? You ask. It is object variable. In fact it is
object attribute. It is available in whole object, in every method of object.

We can refactor `full_name` method:

```ruby
def full_name
  return "#{@first_name} #{@last_name}"
end
```

In `initialize` we set its value and in `full_name` we get its value. Attributes
/ object variables can be reached only from inside the object, you can use them
only inside the class.

I said that `attr_accessor :first_name` creates the attribute and it's public.
I kinda lied. `attr_accessor` creates an accessor to the attribute, attribute
is private, but can be reached via accessor which is public. In fact accessor
is a regular method.

This

```ruby
class Car
  attr_accessor :color
end
```

is exactly the same as this

```ruby
class Car
  def color
	 return @color
  end

  def color=(new_color)
    @color = new_color
  end
end
```

`attr_accessor` is like a macro that creates getter - `color` and setter -
`color=`.

Now, when you create an visitor you must pass an argument to method `new`.
In our case:

```ruby
visitors << Visitor.new(response)
```

|version|
|:-----:|
|  1.3  |

Names does not change
---------------------

Our class allows for reading the names, which is understandable, but it also
allows for changing the names. And as we know names doesn't change, at least not
inside our headquarter... We need to delete setter for first and last name.
Instead of creating accessors we can create only reader:

```ruby
attr_reader :first_name, :last_name
```

|version|
|:-----:|
|  1.4  |

Don't stay to long
------------------

Visitors should not be inside longer that 8h. To check this we need to track
time. We need to keep time of entrance, we can assign it in `initialize`

```ruby
def initialize(full_name)
  # ...
  @entry_time = Time.new
end
```

We need method that will count duration time.

```ruby
def duration
  Time.now - @entry_time
end
```

We need to display more information that just full name, we need another method
`status` that will display duration time and alert if duration is greater than
8 hours.

```ruby
def status
  time = duration
  alert = "Longer than 8h!" if time > 60 * 60 * 8
  return "#{time} #{alert}"
end
```

We should display duration time in more readable format, right now it is float
number of seconds. Create `pretty_time` method:

```ruby
def pretty_time(time)
  hours = time / (60 * 60)
  minutes = time / 60 % 60
  seconds = time % 60
  '%02d:%02d:%02d' % [hours, minutes, seconds]
end
```

Use this method in `status`.

One more thing, in our main program file in method `print_state` we should
print person status.

```ruby
puts "#{index} - #{person} - #{person.status}"
```

|version|
|:-----:|
|  1.5  |

Keep your private stuff private
-------------------------------

Right now every method of class `Visitor` is public. But only `full_name` and
`status` are used outside the class - they are called on the object.

> `initialize` is private by default, you can't call it on an object.

Other methods are used internally, only inside the class. Though your program
works fine with all methods public it is a bad design. Methods that are not
used outside make private.

Methods after word `private` are private:

```ruby
class SomeClass
  def method_one
    puts "I'm prublic"
  end

  private

  def mthod_two
    puts "I'm private"
  end
end
```

So, put `private` in `Visitor` and make methods `duration` and `pretty_time`
after word `private`.

When you make some methods private you make whole class easier to refactor.
When someone else or feature you decides to change the class he/she basically
can't modify public methods or must change every call of those methods
(sometimes it's literally impossible). Private methods are much easier to
change or delete because you have every usage of those in place.

|version|
|:-----:|
|  1.6  |

Let visitors in
---------------

Let's say we need to let guests in, not visitor but outside guest.
Guest must give their phone number. We could write another class `Guest`
but we would repeat most of the code from `Visitor`. DRY rule forbids this.
We could just add additional features to `Visitor` and in special cases check
if we have regular visitor or guest in object. When you need to use term "check
for special cases" for 99% this means your design is bad, like really bad.
Imagine situation when you need to add another type of person - every `if` -
`else` become `if` - `elsif` - `else`, and what on another type?

You can create class that extends other class. This feature of language is called
Inheritance, it is typical for object oriented languages.

Create new file *guest* (in *lib* dir) with  empty class `Guest` in it.
Require it on main file.

```ruby
require_relative 'lib/guest.rb'
```

Empty `Guest` class:

```ruby
class Guest
end
```

Extend it with `Visitor` class.

```ruby
require_relative 'visitor.rb'
class Guest < Visitor
end
```

Now `Guest` extends `Visitor`. That means `Guest` have every methods and
attributes of `Visitor`. Right now, before you add any new method to `Guest`,
those classes are the same, you could use them interchangeably.

> `Visitor class can be called a **super class** for `Guest` class or
> **parent class** for `Guest` class. Also we say that `Guest` inherits all
> methods form parent class (and of course form grand parent, and so on)

But the point is to extend behavior of `Visitor`. We need phone number attribute
(read only)

```ruby
attr_reader :phone_no
```

We want to set phone number on `initialize`. Right now `Guest` have `initialize`
inherited from `Visitor`, we need to rewrite it, add another parameter
`phone_no`, we also need to implement names operations, same as those in
`Visitor`. New `initialize` could look like this:

```ruby
def initialize(full_name, phone_no)
  names = full_name.split(' ')
  @first_name = names.first
  @last_name = names.last
  @entry_time = Time.new
  @phone_no = phone_no
end
```

Name related stuff are exactly the same as in the super class. We can actually
call `initialize` method from super class by using keyword `super`.

```ruby
def initialize(full_name, phone_no)
  super(full_name)
  @phone_no = phone_no
end
```

> `super` calls same method from super class (parent class).

We need to change `to_s` behavior. We introduce new method `full_info`

```ruby
def full_info
  return "#{full_name} (#{@phone_no})"
end
```

And redefine `to_s` alias:

```ruby
alias to_s full_info
```

We have class ready, we need to modify program.

Require new class.

```ruby
require_relative 'lib/guest.rb'
```

Modify message in method `user_interaction`

```ruby
puts 'Enter persons number to remove'
puts 'or enter name to add new'
puts 'or enter name "/" phone_no to add a guest (eg. Joe Shith/223 33 55)'
puts 'or enter "q" to quit'
```

And most important, change adding visitors:

```ruby
full_name, phone_no = response.split('/')
if phone_no
  visitors << Guest.new(full_name, phone_no)
else
  visitors << Visitor.new(full_name)
end
```

| version |
|:-------:|
|   1.7   |

<!--  LocalWords:  stdlib polymorphism accessor getter
 -->
