HTTP
====

HTTP is a protocol for communication between web browser and web server. Lets
analyze simple scenario:

Scenario: User enters "http://my-news.com/today/sport.html" into address input
of the browser.

Whats going on here.

1. The browser sends a request to the server on address "my-news.com" at port 80.
   If port isn't specified, the default is used, which is 80 (for non-encrypted
   communication).

   HTTP Request is a multi line string. Each line is ended with "\r\n". HTTP
   Request consists of two parts, first one is the header and second is the body.
   Header is separated from body by empty line "\r\n". Body is optional, there
   can be a request with header only (and empty line at the end).

   First line of the header is special, it consists of three values separated by
   space. First value is a Method, second one is a URI and the third is a
   Protocol Version.

   Successive lines of the header are parameters. In format: parameter name,
   colon, space, value.

   In Our case HTTP Request will looks like this:

   ```
   GET /today/sport.html HTTP/1.1
   Host: my-news.com:80
   User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
   Accept-Language: pl,en;q=0.5
   Accept-Encoding: gzip, deflate

   ```

   This HTTP Request with Method GET, Request with method GET don't have Body.
   Method GET says to the server "give me a resource, that is specified by URI".
   Parameters describe few circumstances, like what language the user
   understands, what browser is making this request or what type of compression
   the browser supports.
2. Server receives the request. It finds resource identified by URI. In this
   case it is a recent sport news document in HTML format.

   Server composes a HTTP Response.

   HTTP Response is similar to HTTP Request, it consists of the header and the
   body separated by empty line. Each line ends with "\r\n". First line of the
   header is special, it consist of three values separated with spaces. The
   values are (in order): Protocol Version, Status Code, Status Message.
   Successive lines are parameters in same form as in request.

   In our case HTTP Response composed by the server will look like this:

   ```
   HTTP/1.1 200 OK
   Content-Type: text/html; charset=utf-8
   Content-Length: 164

   <html>
     <head>
       <title>Sport News</title>
     </head>
     <body>
       <h1>Today in sport!</h1>
       <p>Dynamo Barcelona vs Grenade Madrid - 6:1</p>
     </body>
   </html>
   ```

   Our HTML document is placed in body of the response. Server must inform the
   browser what the body is. Server places two parameters in the Header:
   Content-Type, which informs what type of document is in body and
   Content-Length, which informs how long the body is.

   In the first line server informs the browser that everything if OK.
3. Browser receives the response. It analyze the header and knows that it should
   display HTML document, which is in body of the response.

Let's write our own HTTP server
-------------------------------

> Example project is at https://gitlab.com/learn_to_code/http_playground

We will use a socket. Socket is thing that provides a communication channel over
networks. It is a server-client communication, where server constantly listens
for incoming messages and can respond for each message once. Communication is
between two entities only, that means if one client starts talking to a server
other clients must wait for free channel.

Socket by itself is similar to a file, in fact it is a file, in fact in Unix
kind operating systems everything is a file, but it is a story for another time.

So, you can read a socket (like a file), and you can write to a socket (like to
a file). If socket is establish between program A and program B, what program A
will write to a socket program B can read, and, what program B will write
program A can read.

Create file *dummy_server.rb*

```ruby
require 'socket'

host = 'localhost'
port = 2345
server = TCPServer.new(host, port)

loop do
  socket = server.accept
  puts "\nI'm listening on http://#{host}:#{port} (hit Ctrl+C to exit)\n"
end
```

This code is responsible for creating a socket on `host` at `port`. Don't run it
yet right now it will create infinite loop.

Add code that will receive HTTP Request from the browser.

```ruby
loop do
  # ...

  http_request = ""
  loop do
    line = socket.gets
    http_request += line
    break if line == "\r\n"
  end

  puts "\nI received a HTTP Request from the browser:"
  puts http_request
end
```

Add code that will analyze the request.

```ruby
loop do
  # ...

  method, uri = http_request.lines.first.split(' ')
  if method == 'GET' && uri == '/random_number.html'
    sentence = "This is random number between 0 and 100: #{rand 100}"
  else
    sentence = 'This is non random number: 1'
  end
end
```

Program creates variable `sentence`, which is a message for user, and it depends
on Method and URI.

Compose HTML document, that should be send back to the browser.

```ruby
loop do
  # ...

  # HTML (not HTTP) content, that should be send to the browser, this is what
  # the user want to see
  html_content =
    "<html>\n" \
      "<head>\n" \
        "<title>Dummy Server</title>\n" \
      "</head>\n" \
      "<body>\n" \
        "<h1>Hello World!</h1>\n" \
        "<p>#{sentence}</p>\n" \
      "</body>\n" \
    "</html>\n"
end
```

Compose complete HTTP Response:

```ruby
loop do
  # ...

  http_response_header =
    "HTTP/1.1 200 OK\r\n" \
    "Content-Type: text/html; charset=utf-8\r\n" \
    "Content-Length: #{html_content.bytesize}"

  http_response =
    "#{http_response_header}\r\n" \
    "\r\n" \
    "#{html_content}"
end
```

Now we are ready to send the response. Let's ask server administrator for a
confirmation before sending the response. (That way our server isn't very
practical since it need an operator, which confirms every response, but it will
demonstrate what is happening).

```ruby
loop do
  # ...

  puts "\nI prepared a response to the browser:"
  puts http_response
  puts "Hit Enter to send response to the browser.\n"
  gets
  socket.print http_response
  puts "Message sent.\n"
  socket.close
end
```

Play with it, for best impression, run program in a window side by side with a
browser window ;-)

| version  |
|:--------:|
| sample/1 |
