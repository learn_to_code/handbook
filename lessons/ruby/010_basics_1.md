Ruby Basics 1
=============

<!-- @ToDo: introduce symbols -->

Lets play few more times...
---------------------------

We want to play three time in a row. Use method from class Fixnum `times`.

```ruby
3.times do
  puts 'loop iteration...'
end
```

> @Todo: add small samples to each new element (like loop) and small excersise

It will printout:

```
loop iteration...
loop iteration...
loop iteration...
```

Apply this loop to your game.

|version|
|:-----:|
|  1.1  |

You can check my sample solution for this version

```sh
git diff --ignore-all-space origin/sample-1.0 origin/sample-1.1
```

> `--ignore-all-space` option hides changes in white characters, like spaces

Be more expressive
------------------

Display information about try number. Example:

```ruby
3.times do |loop_no|
  puts "loop iteration no. #{loop_no}..."
end
```

It will printout:

```
loop iteration no. 0...
loop iteration no. 1...
loop iteration no. 2...
```

`loop_no` is a variable named by you, you can name it whatever you like.

This will do exact same thing:

```ruby
3.times do |count|
  puts "loop iteration no. #{count}..."
end
```

Apply it to your game.

|version|
|:-----:|
|  1.2  |

To check my solution

```sh
git diff --ignore-all-space origin/sample-1.1 origin/sample-1.2
```

Count the big result
--------------------

Record how many times player has won and display final result.

To accomplish this you need two additional variables for counting player score
and computer score. Bellow main loop display:
* score of player and score of computer
* information who won in score

|version|
|:-----:|
|  1.3  |

My casino - my rules
--------------------

Allow user to define how many times they want to play. Use argument passed in
command line. User will run game like this to play 5 times:

```sh
ruby rock-paper-scissors.rb 5
```

or like this if script is executable:

```
./rock-paper-scissors.rb 5
```

If user doesn't pass any arguments, there should be three tries by default.

Ruby has special Array with all arguments passed from command line `ARGV`.
`ARGV[0]` is a first argument, `ARGV[1]` second, and so on... All elements of
ARGV are String class instances, even if they looks like numbers. `"5"` is not
the same as `5`. String has `to_i` method that converts string to number
(Fixnum).

To check if an Array is empty call 'empty?' method. `my_array.empty?` will
return `true` if there is no elements in the Array.

> In Ruby methods can have "?" and "!" character at the end. Technically it
> doesn't do any special, but the convention is that methods ended with "?"
> should return boolean value. "!" character is added to methods potentially
> destructive, so be careful when you call such methods (just be sure what they
> do).

Single `!` is **not** operator, it negates given value, it converts `true` to
`false` and `false` to `true`.

You have all the puzzles, code!

|version|
|:-----:|
|  1.4  |

As long as you want
-------------------

Allow user to play as long as he or she wants. To end the game user should type
letter 'q' as their move. To accomplish this we need to replace our loop with
fixed number of iterations to loop with condition. This type of loop is `while`

`while` loop works like this:

```ruby
age = 0
while age < 18
  puts 'wait...'
  age += 1
end
puts 'You can buy a bear.'
```

Test it.

Replace loop `times` with `while` in your game.

|version|
|:-----:|
|  1.5  |

Pretty printing
---------------

We want prettier output. Important stuff should be printed with ornaments, like
this:

```
            __-o8o-__
*================================*
This is try no. 0
To set your handing, enter letter:
"r" for rock
"p" for paper
"s" for scissors
*================================*
            ~~=====~~

```

To print such things, just `puts` it:

```ruby
puts '            __-o8o-__'
puts '*================================*'
```

And bottom one similar, but with additional empty line

```ruby
puts '*================================*'
puts '            ~~=====~~'
puts
```

Go and add those ornaments to info before each try intro, each try result and
final result.

|version|
|:-----:|
|  1.6  |

DRY
---

DRY is aa ancient rule of development. it means Don't Repeat Yourself. Why it's
important? Because we need change our ornament, from

```
            __-o8o-__
*================================*
```

to

```
            ..-o^o-..
*================================*
```

> Don't do it right now. If you simply change this in every place you will
> repeat youself and at least one hacker will die.

Use method.

```ruby
def ornament_head
  puts '            __-o8o-__'
  puts '*================================*'
end
```

`def` is a key word to define a method, next thing is method name,
*ornament_head* in this example. Then there is a body of the method until the
`end` keyword. Then every time you want put the ornament call the method by its
name.

```
ornament_head
```

Add this method to your game. Define also `ornament_tile` method, that will
print bottom part of the ornament. Then replace every code that produces the
ornaments in the game with methods calls.

Method definition must be placed before its usage, so place ours at the top,
above the variables.

Now you can change the ornament, you are doing it once, it easier and safer.
You won't miss any occurrence of ornament in the code.

|version|
|:-----:|
|  1.7  |

Super power methods
-------------------

Method can get super power, it can get arguments. Like this:

> @FixMe: this example is lame, re-think it

```ruby
def can_vote?(age)
  if age >= 18
    return true
  end
  false
end
# try it
if can_vote?(15)
  puts 'I can vote!'
else
  puts "I can't vote :("
end
```

Try it with different arguments.

We need to paint ornaments with different length. Strings in Ruby can be
multiplied

```ruby
'-' * 20
```

and added

```ruby
'foo' + 'bar'
```

Modify ornament methods that they get argument `length` and print ornament of
given length.

Then set argument length 40 to all ornaments except for round result.
For round result set length 15.

|version|
|:-----:|
|  1.8  |

Default value
-------------

We can define default value of method argument:

```ruby
def ornament_head(length = 40)
  spaces = ' ' * (length / 2 - 4)
  puts spaces + '..-o^o-..'
  bar = '=' * (length - 2)
  puts '*' + bar + '*'
end
```

Then when you call the method without argument, the argument will have default
value. Modify our methods, give 40 as default value of length, and call them
without argument where it is now 40.

|version|
|:-----:|
|  1.9  |

More arguments!
---------------

Method can get more than one argument, in fact method can get any number of
arguments.

```ruby
def introducing(name, country)
  puts "My name is #{name}, I live in #{country}."
end
```

Add another argument to ornament methods. We need to change style of ornament.
Right now belt is made of '=' chars, we have to be able to use any char.

And one more thing, arguments without default value must be placed before the
ones with default value. Like this:

```ruby
def introducing(name, country = 'Kabuto')
  puts "My name is #{name}, I live in #{country}."
end
```

So, add `char` argument and use it as char of which bar is built. Then use char
'-' to print ornament in all places except the final result, there use '='.

|version|
|:-----:|
|  1.10 |

DRY, DRY, DRY!
--------------

There are part of same code in the `ornament_head` and `ornament_tail`. Lets
extract it to separate method. Code responsible for producing the bar can be
extracted into method.

```ruby
def bar(char, length)
  bar = char * (length - 2)
  puts '*' + bar + '*'
end
```

Use this method inside ornament methods.

|version|
|:-----:|
|  1.11 |

Generating spaces is also the same. Extract it.

```ruby
def spaces(length)
  ' ' * (length / 2 - 4)
end
```

This method returns a value (String in this case). `bar` method prints (`puts`)
some string to standard output, it returns nothing (`nil`). So, in both cases
usage is different. Example usage:

> @ToDo: Example explaining returning value by method

```ruby
bar('~', 10)
puts spaces(10)
```

|version|
|:-----:|
|  1.12 |

Methods are not only about re-usage
-----------------------------------

Very often method is used to clarify the code. In our game we can extract code
responsible for user interaction to a method:

```ruby
def player_interaction
  ornament_head '-'
  puts 'To set your handing, enter letter: '
  puts '"r" for rock'
  puts '"p" for paper'
  puts '"s" for scissors'
  ornament_tail '-'
  puts 'Enter "q" to quit.'
  STDIN.gets.chomp
end
```

Then in the main loop:

```ruby
player = player_interaction
```

|version|
|:-----:|
|  1.13 |

It's not much improvement, but it allow us to do more clarification. Checkout
sample 1.14

```sh
git diff --ignore-all-space origin/sample-1.13 origin/sample-1.14
```

or checkout whole file:

```sh
git show origin/sample-1.14:rock-paper-scissors.rb
```

|version|
|:-----:|
|  1.14 |

We have here this:

```ruby
input != 'q' ? input : false
```

This is ternary operator, it works like that:

```ruby
condition ? value1 : value2
```

This expression returns `value1` if `condition` is true, otherwise it returns
`value2`. Ternary operator is often used for assignment, like:

```ruby
final_value = condition ? value1 : value2
```

> **[!]** Build only short expressions with ternary operator, if it's too long
> it's hard to read.

What does `do` do?
------------------

`do` does block. Block of code. What is block of code? Hard to explain...

We have an Array:

```ruby
numbers = [1, 2, 5, 8, 12, 13]
```

We can extract from this Array some subset e.g. all even numbers, we know that
result of modulo 2 operation on even number is 0.

In Ruby modulo operator is `%`. E.g. `number % 2`. If it's 0 `number` is even.
Array have `select` method, which selects some subset of its elements. But what
elements? You decide:

Even elements:

```ruby
numbers.select do |item|
  item % 2 == 0
end
```

short version:

```ruby
numbers.select { |item| item % 2 == 0 }
```

So, method `select` selects those elements for which given block returns `true`.
There is one argument for block (`item` in this case), you name this argument.
It could be `element`, `i` or anything else.

You can select anything.

Elements greater than 8:

```ruby
numbers.select { |item| item > 8 }
```

Own method with block
---------------------

Back to our game.  There are few elements displayed with ornaments, like this:

```ruby
ornament_head 15
puts 'some useful information'
ornament_tail 15
```

It error prone, it's easy to put different length... We need method, which we
can use like that:

```ruby
ornament('-', 15) do
  puts 'some useful information'
end
```

Define such method:

```ruby
def ornament(char, length = 40)
  ornament_head(char, length)
  yield
  ornament_tail(char, length)
end
```

and replace calls of `ornament_head` and `ornament_tail` with `ornament`.

|version|
|:-----:|
|  1.15 |

It's time to code more
----------------------

Develop another game. This time by yourself.
