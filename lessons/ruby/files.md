Files
=====

Make a project for this lesson and write in some files examples shown here.

Procedural style
----------------

### write a file

Create a file *write_my_story.rb*.

```ruby
file_path = "#{ENV['HOME']}/my_story.txt"
my_story = "This is my story.\n"\
           "I'm learning how to code,\n"\
		   "not because it's easy,\n"\
           "but because it's super cool!\n"
story_file = File.open(file_path, 'w')
story_file.write(my_story)
story_file.close
```

`File.open` takes two arguments - path to a file, and mode - 'w' for write, 'r'
for read, 'rw' for read and write.

`story_file` is a file handler, it is object of class `File`, it has plenty of
methods that allows you to manipulate a specific file, `write` is one of them.
Class `File` has also class methods, which you can use directly form class
(not form object of this class) e.g. `File.open`.

> `ENV` is a constant that keeps all environment values, about operating system,
> user that runs the program, Ruby and other. `ENV['HOME']` keeps path to
> current user home directory.

Open file *~/my_story.txt* in text editor ("~/" means your home directory ;-).

### read a file

Create another file, e.g. *read_my_story.rb*

```ruby
file_path = "#{ENV['HOME']}/my_story.txt"
story_file = File.open(file_path, 'r')
my_story = story_file.read
story_file.close

puts my_story
```

### read file line by line

Create another file - *line_by_line.rb*

```ruby
file_path = "#{ENV['HOME']}/my_story.txt"
story_file = File.open(file_path, 'r')
story_file.each_line do |verse|
  puts verse
  if verse.include? 'code'
    break
  end
end
story_file.close
```

We break the printing when we encounter a line with "code" substring. Such
approach is useful when we have large file and we actually don't need to read it
all at once. Reading whole file that is large (e.g. few gigabytes) can be very
long and consume huge amounts of RAM.

Ruby style
----------

What does it exactly mean "Ruby style"? In simple words - usage of code blocks.
Other languages (that are widely used) doesn't have code blocks, and it is
hard or/and complex to accomplish similar functionality.
In not simple words - nobody knows, everybody use it.

### write a file

```ruby
file_path = "#{ENV['HOME']}/my_story.txt"
my_story = "This is my story.\n"\
           "I'm learning how to code,\n"\
		   "not because it's easy,\n"\
           "but because it's super cool!\n"

File.open(file_path, 'w') do |story_file|
  story_file.write(my_story)
end
```

You don't have to worry about closing the file.

### read a file

```ruby
file_path = "#{ENV['HOME']}/my_story.txt"
File.open(file_path, 'r') do |story_file| 
  my_story = story_file.read
  puts my_story
end
```

### read a  file line by line

Create another file - *line_by_line.rb*

```ruby
file_path = "#{ENV['HOME']}/my_story.txt"
File.open(file_path, 'r').each_line do |verse|
  puts verse
  break if verse.include? 'code'
end
```

Save object in a file
---------------------

Saving text in files is cool, but saving objects is even cooler. In Ruby you
can't save an object in the file, only text. But you can take a "picture" of an
object in text format - it's called serialization.

We will use a Hash as an example, but this works for any kind of object.

You must load YAML library.

```ruby
require 'yaml'
file_path = "#{ENV['HOME']}/my_object.txt"
info = { name: 'Joe', address: 'New York City, USA', skill: 100 }

text = info.to_yaml
File.open(file_path, 'w').write text
```

And load it back
----------------

```ruby
require 'yaml'
file_path = "#{ENV['HOME']}/my_object.txt"

text = File.open(file_path, 'r').read # @FixMe: is this file closed?

info = YAML::load text
puts info
```
