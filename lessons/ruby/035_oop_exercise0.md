OOP Exercise 0
==============

> Example project: https://gitlab.com/learn_to_code/time_keeper

Time Keeper is an app to track time.

Application has a log, it's a list of records. Record - log entry - has public
attributes:
* start_time - time of event start
* type - string, one of: relax, personal, work, recreation, education, other,
  routines, sleep
* tags - array of strings, optional
* description - string, optional

App displays all records and asks user for new record.

Eg.

```sh
2017-04-15 12:20 personal [computer update; Linux] "Installation of Archlinux on laptop"
2017-04-15 14:20 relax [youtube]
2017-04-15 14:20 recreation [walk] "Walk in the park"
Add new record or type "q" to quit
```

Entering new record.

Program asks for a start time, (empty - use current)

```sh
Enter start time (empty - use current)
Date and time: "YYYY-MM-DD HH:MM" or time only "HH:MM" (current date)
_
```

Then program ask for a type, it displays a list with numbers to choose.

```sh
Enter type number
1 - relax
2 - personal
3 - ...
_
```

Then program asks for tags:

```sh
Enter tags, split by ";", empty - no tags
_
```

Then program asks for description:

```sh
Enter description (empty - no description)
_
```

The program
-----------

Create program main file - *time_keeper.rb*

We need log, which is an Array of log entries. Main loop, which will display
all log entries. And we need a way to end the program.


```ruby
#!/usr/bin/env ruby

log = []

loop do
  log.each do |entry|
    puts entry.to_s
  end

  puts 'Enter "q" to quit'
  user_input = STDIN.gets.chomp
  break if user_input == 'q'
end
```

| sample |
|:------:|
|     1  |

LogEntry class
--------------

We need to create a class for log entry objects. Create file *lib/log_entry.rb*

```ruby
class LogEntry
end
```

We need attribute readers for attributes: start_time, type, description and tags

```ruby
class LogEntry
  attr_reader :start_time, :type, :description, :tags
end
```

We need to initialize all the attributes.

```ruby
class LogEntry
  attr_reader :start_time, :type, :description, :tags

  def initialize(type, start_time = nil, description = nil, *tags)
  end
end
```

With arguments like this, we can initialize object in several ways:
* with type only e.g. `entry = LogEntry.new 'relax'`
* with type and start_time
  e.g. `entry = LogEntry.new 'relax', '2017-05-16 12:00'`
* with type, start_time and description
  e.g. `entry = LogEntry.new 'relax', '2017-05-16 12:00', 'watch TV'`
* with type, start_time, description and few tags
  e.g. `entry = LogEntry.new 'relax', '2017-05-16 12:00', 'watch TV', 'tv', 'waste time'`

### attribute type

Attribute type can be one of specified values. Create a constant inside the class
for that purpose, and assign Array of values to it.

```ruby
class LogEntry
  TYPES = [
    'personal',
    'work',
    'recreation',
    'education',
    'routines',
    'relax',
    'other',
    'sleep',
  ]

  attr_reader :start_time, :type, :description, :tags

  def initialize(type, start_time = nil, description = nil, *tags)
  end
end
```

Constant is like variable, but its name starts with capital letter (Yes, class
technically is a constant too). Constant is global. You can access constant that
is defined in a class (like in our case) from outside e.g. `LogEntry::TYPES`.
If you are in the class access constant simply by its name `TYPES`.

By convention constant should be all capital letters. Name 'Types' would work
as well, but it would suggest that it is a class (according to the convention).

Constants are not very constant in Ruby, you can change them - redefine, but
Ruby will issue a warning. You should never redefine a constant unless you are
absolutely know what you are doing (e.g. debugging or manky-patching).

Now we are ready to implement `type` initialization

```ruby
def initialize(type, start_time = nil, description = nil, *tags)
  if TYPES.include? type
    @type = type
  else
    raise 'Invalid type!'
  end
end
```

Method `raise` issues an exception, which will end the program with error.

> Later we will learn how to catch an exception and not terminate the program

### start_time attribute

If `start_time` is given it should be parsed to `Time` object else create `Time`
object with current time.

```ruby
def initialize(type, start_time = nil, description = nil, *tags)
  # ...
  if start_time
    @start_time = Time.parse start_time
  else
    @start_time = Time.new
  end
end
```

To use Time.parse you have to load extended time library (at the beginning of
the file):

```ruby
require 'time'
```

### description attribute

```ruby
def initialize(type, start_time = nil, description = nil, *tags)
  # ...
  @description = description
end
```

If you don't give `description` argument (it will be `nil`), then you will
assign `nil` to `@description` attribute. It's OK, because undefined attributes
have `nil` value anyway.

### tags attribute

We defined tags attribute as `*tags` - that means `tags` is an Array of any
number of values (including no values). And when you call the method you just
add any number of values separated with ','. Eg.
`LogEntry.new 'relax', '2017-05-16 12:00', 'watch TV', 'tv', 'waste time'`

'tv', 'waste time' are elements of Array `tags`.

```ruby
def initialize(type, start_time = nil, description = nil, *tags)
  # ...
  @tags = tags
end
```

| sample |
|:------:|
|     1  |

Display the log
---------------

To use our class in the program require it.

```ruby
require_relative 'lib/log_entry.rb'
```

And create some dummy log entries (After define `log` and before main loop).

```ruby
log << LogEntry.new('work', '2017-05-16 12:00')
log << LogEntry.new('education',
                    '2017-05-16 15:00',
                    'Ruby learning',
                    'ruby',
                    'programming',
                    'computer')
```

Try it.

We need to improve printing of entries.

```ruby
class LogEntry
  # ...
  def to_s
    "#{@start_time} #{@type} #{@tags} #{@description}"
  end
end
```

Try it. It much better, but we will improve it further.

Create private method for prettier printing of `@start_time`.

Add keyword `private` to the class and put all private methods after it.

```ruby
class LogEntry
  # ...
  private
  # ...
  def pretty_start_time
    @start_time.strftime('%Y-%d-%m %H:%M')
  end
end
```

And for `@tags`:

```ruby
class LogEntry
  # ...
  private
  # ...
  def pretty_tags
    if @tags.any?
      "[#{@tags.join('; ')}]"
    else
      ''
    end
  end
end
```

And for `@description`

```ruby
class LogEntry
  # ...
  private
  # ...
  def pretty_description
    if @description
      "\"#{@description}\""
    else
      ''
    end
  end
end
```

Fix `to_s` method:

```ruby
def to_s
  "#{pretty_start_time} #{@type} #{pretty_tags} #{pretty_description}"
end
```

| sample |
|:------:|
|     3  |

Adding log entry
----------------

Ask user for start time (in the main loop):

```ruby
loop do
  # ...
  puts '-' * 80
  puts 'Add new record or type "q" to quit'
  # get start time
  puts 'Enter start time (empty - use current)'
  puts 'Date and time: "YYYY-MM-DD HH:MM" or time only "HH:MM" (current date)'
  user_input = STDIN.gets.chomp!
  break if user_input == 'q'
  if user_input.empty?
    start_time = nil
  else
    start_time = user_input
  end
end
```

Then ask user for the type.

```ruby
loop do
  # ...
  # get type
  puts 'Enter type number'
  LogEntry::TYPES.each_with_index do |type, no|
    puts "#{no} - #{type}"
  end
  type_no = STDIN.gets.chomp.to_i
  type = LogEntry::TYPES[type_no]
end
```

Because type must be one of value defined in constant
`TYPES` in class `LogEntry`, show user available options (with indexes), ask
user for type number, then use value form `TYPES` Array at given number.

To access `TYPES` from outside the class, from main program in this case, use
`LogEntry::TYPES`

Then ask for the tags.

```ruby
loop do
  # ...
  # get tags
  puts 'Enter tags, split by ";", empty - no tags'
  tags = STDIN.gets.chomp.split(';').map { |tag| tag.strip }
end
```

`STDIN.gets.chomp` is known to you, `split(';')` converts String into Array, by
splitting it on every ';' character, then `map { |tag| tag.strip }` converts
each element of this Array with method `strip`. `strip` strips any spaces at the
beginning and end of String. A good idea is to try each method on some test
examples or in the irb session.

Next, ask for description.

```ruby
loop do
  # ...
  # get description
  puts 'Enter description (empty - no description)'
  user_input = STDIN.gets.chomp
  if user_input.empty?
    description = nil
  else
    description = user_input
  end
end
```

And finally create the entry and add it to the log.

```ruby
loop do
  # ...
  log << LogEntry.new(type, start_time, description, *tags)
end
```

We use `*tags` because, tags should be given as a list of additional arguments,
like our dummy entries at the beginning, but we have array of values.

Checkout this example.

```ruby
def print_names(*name_list)
  # name_list is an array, you can pass any number of arguments to this method
  name_list.each do |name|
    puts name
  end
  puts
end

# use case 1

print_names('Spiderman', 'Batman', 'Robocop')

students = ['Monica', 'Joe', 'Mario']
# when you have an array but you should pass multiple arguments use '*' char
print_names(*students)
```

Remove dummy log entries form the beginning of the program.

| sample |
|:------:|
|    4   |

Classify main program
---------------------

We've made nice class for the log but main program is still procedural, lets
convert it to OOP too.

Create program class file *lib/time_keeper.rb*

```ruby
class TimeKeeper
end
```

We will just move the functionality to this class.

Create `initilize` method and initialize object variable `@log`.

```ruby
class TimeKeeper
  def initialize
    @log = []
  end
end
```

Create method `run` that contains main loop.

```ruby
class TimeKeeper
  # ...
  def run
    loop do
      @log.each do |entry|
        puts entry.to_s
      end

      puts '-' * 80
      puts 'Add new record or type "q" to quit'
      # get start time
      puts 'Enter start time (empty - use current)'
      puts 'Date and time: "YYYY-MM-DD HH:MM" or time only "HH:MM" (current date)'
      user_input = STDIN.gets.chomp!
      break if user_input == 'q'
      if user_input.empty?
        start_time = nil
      else
        start_time = user_input
      end

      # get type
      puts 'Enter type number'
      LogEntry::TYPES.each_with_index do |type, no|
        puts "#{no} - #{type}"
      end
      type_no = STDIN.gets.chomp.to_i
      type = LogEntry::TYPES[type_no]

      # get tags
      puts 'Enter tags, split by ";", empty - no tags'
      tags = STDIN.gets.chomp.split(';').map { |tag| tag.strip }

      # get description
      puts 'Enter description (empty - no description)'
      user_input = STDIN.gets.chomp
      if user_input.empty?
        description = nil
      else
        description = user_input
      end

      @log << LogEntry.new(type, start_time, description, *tags)
    end
  end
end
```

We need to require log entry class in this file (at the beginning).

```ruby
require_relative 'log_entry.rb'
```

Fix main program file. Remove all the content, it was moved to separate class.
We need to initialize object of that class and "run" it.

```ruby
require_relative 'lib/time_keeper.rb'

app = TimeKeeper.new
app.run
```

> BTW. Things that we made here is called "refactoring", we've change the
> program, but in point of user view (functionality) it works exactly the same
> way. Purpose of this refactoring is code quality improvement. It is easier to
> develop the program than before our changes.

| sample |
|:------:|
|     5  |

Save the log
------------

If it's a log it must be saved. Lets save it in the file.

Read about files at (/lessons/ruby/files.md).

We need to save the log on every change (if program crashes most of work will be
saved). We could simply add saving of `@log` after adding each entry, but this
is separate functionality so it should be in some other class.

Saving the log is strictly coupled with the `@log` object. Although it `Array`
we can extend its functionality. We will create new class for the log.

*lib/log.rb*

```ruby
class Log < Array
end
```

Then use it instead of `Array` in `TimeKeeper` class, method `initialize`.

```ruby
@log = Log.new
```

Right know nothing changed. class `Log` has exactly the same functionality as
class `Array`, but we are free to extend it or change its present behavior.

> In fact, in Ruby, you can modify behavior of any class even build-in one, like
> `Array` - BUT - never do that. This is a special and dangerous feature of
> Ruby that should used only by adults.

Let's create a naive implementation of log saving. Modify `<<` method:

```ruby
class Log < Array
  # ...
  def << entry
    super
    File.open(@file_path, 'w') { |file| file.write self.to_yaml }
  end
end
```

`super` calls method of the same name (`<<`) of parent class (`Array`). Next
line saves `self`, which is the object of class `Log` (`@log`) in file in
`@file_path`.

We use `YAML` we need to require the library (at the beginning of file).

```ruby
require 'yaml'
```

Next we need to initialize `@file_path`.

```ruby
class Log < Array
  def initialize(file_path)
    @file_path = file_path
  end
  # ...
end
```

In the initializer we also need to load data from file and "put" it in `self`.

```ruby
class Log < Array
  def initialize(file_path)
    @file_path = file_path
    File.open(@file_path, 'r') do |file|
      data = YAML.load file.read
      self.replace data
    end
  end
  # ...
end
```

Now, create the log object with file path:

```ruby
class TimeKeeper
  def initialize
    @log = Log.new("#{ENV['HOME']}/time_log.yaml")
  end
  # ...
end
```

| sample |
|:------:|
|     6  |

Dependency Injection (DI)
-------------------------

This works, but... what if we want change the file format? OK, we can change it.
But what if we want to use multiple file formats (e.g. txt, binary) or maybe
we want to save log into database or system log... it gets complicated.

We say that our `Log` class depends upon of saving mechanism. Right now this
dependency is coupled with the log.

Create separate class for saving the log. `YAMLStorage` *lib/yaml_storage.rb*

```ruby
require 'yaml'

class YAMLStorage
  def initialize(path)
    @path = path
  end
end
```

We need method `load` in it:

```ruby
class YAMLStorage
  # ...
  def load
    data = nil
    File.open(@path, 'w') {} unless File.exist? @path
    File.open(@path, 'r') do |file|
      data = YAML.load file.read
    end
    return data
  end
end
```

`File.open(@path, 'w') {} unless File.exist? @path` this creates an empty file
if it doesn't exist.

And method `save`

```ruby
class YAMLStorage
  # ...
  def save(object)
    File.open(@path, 'w') { |file| file.write object.to_yaml }
  end
end
```

Now we need to modify class `Log`, that it use the storage object.

```ruby
class Log < Array
  def initialize(storage)
    @storage = storage
    data = @storage.load
    self.replace data if data.is_a? Array
  end

  def << entry
    super
    @storage.save self
  end
end
```

We pass storage object to initialize, `Log` saves it on object variable and
uses it later. This is most basic implementation of DI (Dependency Injection) -
`Log` depends on saving mechanism, but it is agnostic about it, we can inject
any object capable of saving and loading data.

Fix `TimeKeeper`

```ruby
require_relative 'yaml_storage.rb'

class TimeKeeper
  def initialize
    storage = YAMLStorage.new("#{ENV['HOME']}/time_log.yaml")
    @log = Log.new(storage)
  end
  # ...
end
```

We create `storage` object from `YAMLStorage` and inject it to `@log` object.

| sample |
|:------:|
|   6.1  |

Another storage
---------------

To demonstrate how we can inject different dependency, let's create another
storage. This time it will be a classic text log, one entry - one line, human
readable.

*lib/txt_storage.rb*

```ruby
class TXTStorage
  def initialize(path)
    @path = path
  end
end
```

Add `load` method, and necessary helper methods:

```ruby
class TXTStorage
  # ...
  def load
    data = []
    File.open(@path, 'w') {} unless File.exist? @path
    File.open(@path, 'r') do |file|
      data = file.each_line.map { |line| create_entry(line) }
    end
    data
  end

  private

  def create_entry(line)
    values = parse_line(line)
    LogEntry.new(
      values[:type], values[:start_time], values[:description], *values[:tags]
    )
  end

  def parse_line(line)
    vals = line.match %r{
      (?<time>\d{4}-\d{2}-\d{2}[[:space:]]\d{2}\:\d{2})
      [[:space:]]
      (?<type>\w+)
      [[:space:]]
      (\[
        (?<tags>.+)
      \])?
      [[:space:]]
      ("
        (?<description>.+)
      ")?
    }x
    type        = vals['type']
    start_time  = vals['time']
    tags        = vals['tags'] && vals['tags'].split('; ')
    description = vals['description']
    { type: type, start_time: start_time, tags: tags, description: description }
  end
end
```

`parse_line` uses Regex to parse the line.

Add `save` method:

```ruby
class TXTStorage
  # ...
  def save(enum_object)
    File.open(@path, 'w') do |file|
      file.write enum_object.map { |i| i.to_s }.join("\n")
    end
  end
end
```

Now we will apply new storage in `TimeKeeper`:

```ruby
require_relative 'txt_storage.rb'

class TimeKeeper
  # storage = YAMLStorage.new("#{ENV['HOME']}/time_log.yaml")
    storage = TXTStorage.new("#{ENV['HOME']}/time_log.txt")
    @log = Log.new(storage)
  # ...
end
```

| sample |
|:------:|
|   6.2  |

How long it took?
-----------------

We know when an entry has begun, but we need to know how long it took.
We assume that every activity ends when next begin.
