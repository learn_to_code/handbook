OOP Exercise 1
==============

We will create an application for point of sale, for a shop, store etc.

Example program is here: https://gitlab.com/learn_to_code/shop

First we need to build some UI (User Interface) framework. We will call it
*Menu* it will display actions to do like this

```
==> Some title
a - action to do
b - another action
q - go back or quit
```

Program will read `STDIN`, it should be one of given options ("a", "b" or "q").
Then program will execute the action.

Menu class
----------

`Menu` class specification:

* `Menu` class has title attribute, read only, set at initialize
* `Menu` class has a method `call` that implements user interaction loop.
  Loop step prints items, reads response, and calls `call` method of the item.
  Loop end when user inputs "q"
* `@items` is an object variable, it's a hash. Key of this hash is a letter that
  user should input to execute the items action. `@items` should contain objects
  (values) that implements method `call`. Method `call` is called as an action
  execution. `Menu` object can be an item - it has a `call` method.

Make a class file *lib/menu.rb*.

```ruby
# CLI framework
class Menu
  attr_reader :title
  def initialize(title, items = {})
    @title = title
    @items = items
  end

  def call
    while (response = user_interaction) != 'q'
      if @items.include? response
        @items[response].call
      else
        puts "#{response} unknown action"
      end
    end
  end

  alias to_s title # will need it in next step but we will also need to define title(def title)

  private

  def print_menu
    puts "\n==> #{@title}"
    @items.each do |key, item|
      puts "#{key} - #{item}"
    end
    puts 'q - to go back or quit'
    print 'enter action: '
  end

  def user_interaction
    print_menu
    STDIN.gets.chomp
  end
end
```

`Menu` has attribute title.
`Menu` has `initialize` method that sets `title` and `items`.
`Menu` has `call` method, that implements user interaction loop.
`Menu` has `to_s` method (an alias in this case) that simply returns the title.

|sample|
|:----:|
|   1  |

Test run
--------

Our CLI framework is ready, we can try it. We can't just run class, so we will
write simple script that uses the class.

Make a commit, if you didn't. We will discard changes made in this step.

Write a test script *dummy_app.rb*

```ruby
require_relative 'lib/menu.rb'

class HelloWorld
  def call
    puts "Hello world!"
  end

  def to_s
    "Hello World"
  end
end

class NowIs
  def call
    puts "Now is #{Time.now}"
  end

  def to_s
    "Now Is"
  end
end

main_menu = Menu.new('Dummy App', 'h' => HelloWorld.new, 'n' => NowIs.new)

main_menu.call
```

Run it, test it.

|  sample |
|:-------:|
| 1.test1 |

Test nested menu
----------------

Make two more class for items.

```ruby
class Fruits
  def call
    puts "Banana, Apple, Mangosteen"
  end

  def to_s
    "Fruits"
  end
end

class Meat
  def call
    puts "Beef, Pork, Chicken"
  end

  def to_s
    "Meat"
  end
end
```

Instead of putting it into `main_menu`, create new menu:

```ruby
sub_menu = Menu.new('Food', 'f' => Fruits.new, 'm' => Meat.new)
```

and add this menu as a sub menu to main menu

```ruby
main_menu = Menu.new('Dummy App',
                     'h' => HelloWorld.new,
                     'n' => NowIs.new,
                     'f' => sub_menu)
```

Run it, test it.

|  sample |
|:-------:|
| 1.test2 |

Discard test changes
--------------------

 <!-- @ToDo: move this to git tutorial -->

You've written a dummy app to test your `Menu` class, but this is not best way to
test your classes. We will cover this subject latter. For now I suggest to delete
dummy app. You could simply delete the file, and commit the changes. In this
simple case is good solution, but what will you do when there is multiple changes
in multiples files? `git revert`!

Example:

If you have repository with commits like that:

```
commit 555_newest
commit 444
commit 333
commit 222
commit 111_oldest
```

> `git log` to check your repository log.

then this will revert commits 333,444,555

```sh
git revert 222..555

```

`222..555` means range from 222 to 555 (without 222).

Revert operation doesn't remove the commits, it creates new commits, with changes
exactly opposite to given commits. E.g. if commit "zxc" adds new line with text
"hello world", then revert operation of this commit will create new commit that
removes "hello world" line.

Back to our case, revert all changes that you've created from "Test run"

Use Menu class
--------------

Create a ruby program file *shop.rb*. First we need to require_relative the Menu
class file. Then create a dummy menu like this:

```ruby
main_menu = Menu.new('Shop',
                     'c' => 'Categories',
                     't' => 'Transactions',
                     'b' => 'Balance')
```

We've created menu object, it's object of class `Menu`. To actually run the menu
you need to call method `call` of this object.

If you try to execute any action other than 'q' program will crash with
exception. That's OK for now we will fix it later.

> Exception is an error that occurs in run time (when program runs), that is not
> syntax error. Syntax error could be detected before running the program, e.g.
> while writing the program if your editor has that feature.

|  sample |
|:-------:|
|    1.1  |

Make some action
----------------

> @ToDo: Change Action.call method to `run` or `run_action`
> @ToDo: Add some runtime action to example action e.g. current date

Make some (almost) real action in our program we need special object that we can
pass as an action. Right now we are passing String - `'b' => 'Balance'` - it
causes an exception because `String` doesn't have `call` method. We need a class
that has `call` method.

*lib/action.rb*

```ruby
class Action
  def call
	puts 'This is some action...'
  end
end
```

Use it in the program - replace 'Balance':

```ruby
'b' => Action.new
```

Now wen you choose 'b' action is taken, but the label is ugly. Fix it. Create an
attribute that will be initialized by first argument of `initialize` and then
returned by `to_s`

```ruby
class Action
  def initialize(label)
    @label = label
  end

  def to_s
    @label
  end
  # ...
end
```

Modify creation of the object.

```ruby
'b' => Action.new('Balance')
```

|  sample |
|:-------:|
|    1.2  |

Make some real action
---------------------

Action of 'Balance' is not the action we want, we could modify method `call`
of class `Action` that it would print the balance, but then it would be usable
only for balance. Action should be general purpose class.

First, small refactoring. Create dedicated variable for object "Balance".

```ruby
balance = Action.new('Balance')
# ...
       'b' => balance
```

|  sample |
|:-------:|
|   1.2.1 |

So, we would like to create action like this:

```ruby
balance = Action.new('Balance') do
  puts '==< The Balance >=='
  puts '+~~~~~~~~~~~~~~~~~+'
  puts '|         $+1000  |'
  puts '+-----------------+'
end
```

By doing this, we pass a block to `initialize`. We need to manage it.

Go to `Action` class, add second argument to `initialize` named `&action`.
"&" char at the beginning means that this argument must be a block of code, and
always must be as the last argument of the method.

You remember that previously we've managed block of code with key word `yield`
without setting any argument - it was good solution to immediate usage of the
block. Now we must save block in a variable and run it later. So we set an
argument, that represent the block and we assign it to the object variable
`@action`.

```ruby
class Action
  def initialize(label, &action)
    @label = label
    @action = action
  end
  # ...
end
```

Lets run the action in our method `call` of class `Action`. When you want to
execute a block of code that you've assigned to a variable just call method
`call` - in our case

```ruby
@action.call
```

And put it on method `call` of class `Action`. I know, a lot of "action"s and
"call"s ;) It would look like this:

```ruby
class Action
  # ...
  def call
    @action.call
  end
end
```

|  sample |
|:-------:|
|    1.3  |

Category
--------

There are products in the shop, each product is assigned to a category.
We need a class to represent a category.

Specification of class Category
-------------------------------

> @ToDo: change Category.each to each_subcategory
> @ToDO: Change `add` -> `add_subcategory`

* `Category` has attribute `label`, which is initialized in `initialize` by first
  argument.
* `label` attribute is read only.
* `Category` has `id` attribute of `Integer` type.
* `id` is read only.
* `id` is generated automatically and unique across all object of class
  `Category`.
* `Category` has attribute `products`, which we initialize as an empty Array.
* `Category` has method `add` that adds subcategory. Subcategories can be reached
  via `each` method.
* `Category` has method `subcategories?` that returns `true` if there are any
  subcategories.

Specification describes only public methods and attribute accessors. Don't
describe any implementation details, internal variables or private methods.

Now lets implement every point of the specs in order:

First create empty class, make a file *lib/category.rb*.

```ruby
# product category
class Category
end
```

Category label
--------------

* `Category` has attribute `label`, which is initialized in `initialize` by first
  argument.
* `label` attribute is read only.

```ruby
class Category
  attr_reader :label

  def initialize(label)
	@label = label
  end
end
```

Category id
-----------

* `Category` has `id` attribute of `Integer` type.
* `id` is read only.

Add reader for it and for now initialize it with zero.

```ruby
class Category
  attr_reader :label, :id

  def initialize(label)
	# ...
	@id = 0
  end
end
```

id autogeneration
-----------------

* `id` is generated automatically and unique across all object of class
  `Category`.

```ruby
class Category
  attr_reader :label, :id
  @@next_id = 0

  def initialize(label)
	# ...
	@@next_id += 1
	@id = @@next_id
  end
end
```

Variables that starts with `@@` are **class variables**, such variable is
visible across all objects of class in which it is used. `@@next_id` is a
variable for all objects of class `Category`, if you change its value in one
object the change will be immediately visible in all other objects of the class,
including future object.

`@@next_id` is increased in `initialize`, so next object gets bigger `id`.

If class variable must be initialized with some value, it must be done in class
body directly - not in any method.

products
--------

* `Category` has attribute `products`, which we initialize as an empty Array.

```ruby
class Category
  # ...
  attr_accessor :products

  def initialize
    # ...
	@products = []
  end
end
```

subcategories
-------------

* `Category` has method `add` that adds subcategory. Subcategories can be reached
  via `each` method.

We will use attribute `@subcategories` to contain list (Array) of subcategories.
To add subcategory we will create `add` method. Subcategories can be accessed
via `each` method.

```ruby
def add(label)
  @subcategories << Category.new(label)
  return @subcategories.last
end

def each
  @subcategories.each do |category|
    yield(category)
  end
end
```

subcategories?
--------------

* `Category` has method `subcategories?` that returns `true` if there are any
  subcategories.

```ruby
def subcategories?
  @subcategories.any?
end
```

| sample |
|:------:|
|    2   |

Use Category class
------------------

In *shop.rb* add require for `Category` class file. Then create root category:

```ruby
root = Category.new('All categories')
```

Then add two subcategories:

```ruby
root.add('Computers')
root.add('Books')
```

Then print categories:

```ruby
# dummy printing of Categories
puts root.to_s
root.each do |category|
  puts " + #{category.to_s}"
end
```

| sample |
|:------:|
|    3   |

More subcategories
------------------

Add some subcategories to "Computers". To do it, assign "Computers" category to
a variable:

```ruby
computers = root.add('Computers')
```

Then add subcategories to it:

```ruby
computers.add('Laptops')
computers.add('Desktops')
computers.add('Servers')
```

Modify printing of categories. In block of `each` that print a category check if
that category have subcategories, if so, print its subcategories:

```ruby
if category.subcategories?
  category.each do |cat2|
    puts "   + #{cat2.to_s}"
  end
end
```

| sample |
|:------:|
|    4   |

Categories in menu
------------------

Current category display is unusable. We want to use `Menu` to display
`Categories`. We could slightly modify `Menu` class but it would made it
multipurpose class - and we know that class should has single responsibility.

It's time to inherit. We will modify class `Menu` by creating new one, that
extends `Menu` class. Original shape of `Menu` stays intact.

New class will be responsible for displaying special case menu - we will name it
`CategoriesMenu`. Create a file for it (accordingly to the rules)

```ruby
require_relative 'menu.rb'

class CategoriesMenu < Menu
end
```

We need to be sure that `Menu` class is loaded before this class is created, so
we ensure this by require `Menu` class in this file. Class `Menu` is required
two times, ones on main file, second in this, but that's OK, required files are
loaded only once.

Our modifications. Normally `Menu` is initialized with label, and options (Hash)
We need to initialize this menu with object of class `Categories` and options
should be subcategories - objects of class `CategoriesMenu`. When user choose
Subcategory - then program should display menu for this subcategory with its
subcategories as options...

```ruby
def initialize(category)
  items = {}
  category.each do |subcat|
    items[subcat.id.to_s] = CategoriesMenu.new(subcat)
  end
  super(category.to_s, items)
end
```

We construct items from subcategories. Key for item is category id (converted to
String), the item itself is another category menu, where category of this menu
is subcategory.

At the end we call for super method - `initialize` from parent class, with
arguments proper for it.

And apply it to our program. Require class file. Remove dummy printing of
categories. Create an object for categories menu:

```ruby
categories_menu = CategoriesMenu.new(root)
```

And use this object in main menu as "categories".

| sample |
|:------:|
|    5   |

Where are products?
-------------------

Since this is shop app, there must be products. Products are little bit more
complex. We need, class that represents a product - `Product`.

* Product should have attribute `id`, unique, auto-generated (read-only).
* Product has attribute `name`, type String (read-only).
* Product has attribute `producer`, type String (read-only).
* Product has attribute `price`, type Decimal (read/write).
* Each product belongs to some category, but this is not a trait of product
  itself, lets say product should not know in what category it is. Moreover
  product can be in more than one category. And as you remember `Category` class
  has an attribute for products list `products` :)

First implement name and producer attributes. Initialize them in `initialize` and
create accessor for them.

Next thing is a price. In programming languages money require special treatment.
Money is a floating point decimal number, that require arbitrary precision in
arithmetic.

Try this (eg in `irb`):

```irb
1.2 - 1.0
```

Processor can handle fractions in two way - fast but with limited precision -
and with arbitrary precision but slowly. Default way, when you write `1.2`, is
the fast one, it uses class `Float`. But money must be counted precisely for
whatever cost. In Ruby there is a class `BigDecimal` for that purpose. It is in
the stdlib (standard library), so you need to `require` it first.

Example of `BigDecimal` usage:

```ruby
require 'bigdecimal'

x = BigDecimal.new('1.2')
y = BigDecimal.new('1.0')

z = x - y

puts "z is #{z.to_s('F')}"
```

`BigDecimal` object can be created in few ways, one of it is to pass `String`
with decimal representation of wanted number. Other way are in the docs ;-).
`to_s('F')` converts BigDecimal to String of classic decimal representation.

So, implement price of product:

```ruby
attr_accessor :price
# ...
def initialize(name, producer, price)
  # ...
  @price = BigDecimal.new(price)
end
```

Since we would like to print the price in readable to human form we need
additional method:

```ruby
def pretty_price
  @price.to_s('F')
end
```

| sample |
|:------:|
|    6   |

Make some products
------------------

> code is ready

| sample |
|:------:|
|    7   |


<!--  LocalWords: accessors STDIN UI accessor bigdecimal
 -->
