Ruby Basics 0
=============

Example project is on https://gitlab.com/learn_to_code/rock-paper-scissors

> How to [validate project](/lessons/meta/validate_project.md)

Lets make a simple game - Rock-paper-scissors.
Create file *rock-paper-scissors.rb*. At the first line put

```ruby
#!/usr/bin/env ruby
```

This is magic comment, that tells your shell that this script is ruby script.
If you give it execute privilege `chmod +x rock-paper-scissors.rb` then you can
execute it by `./rock-paper-scissors.rb` (assuming you are in the dir of game).

You can also run this script by invoking `ruby` directly:

```
ruby rock-paper-scissors.rb
```

Write first code
----------------

```ruby
puts 'Hello World'
```

> @ToDo: irb :)

Everything between single or double quotation marks `'` or `"` is a **String**.
This program will of course print cool message. `puts` is a method that prints
out given string to **standard output**. If you run program in the console then
this console is standard output.

> [About "Hello World"](/lessons/useful_little_things/coding-rule_book.md#hello-world)

Discard this code and back to our game. Print some message to the user.

```ruby
puts 'To set your hanging, enter letter:'
puts '"r" for rock'
puts '"p" for paper'
puts '"s" for scissors'
```

Get users move
--------------

```ruby
player = STDIN.gets.chomp
```

`STDIN.gets.chomp` reads **standard input** and returns it - user must type string
in the console and hit *enter*. Content written by the user is assigned to
variable `player`. Variable is a storage for data, that you want to use in your
code. Variable has a type, like string, number or object. Variable `player` is a
string, because `STDIN.gets.chomp` returns a string.

Generate computers move
-----------------------

Computers move must be random. `rand(x)` returns random number from 0 up to `x`
(excluding `x` - eg. `rand(3)` returns random number from set [0, 1, 2]).
`x` must be an integer (Fixnum in Ruby).

But `rand` returns a number and we need a string, random string from this set
"r", "p", "s". Create an **Array** that consists of this strings.

```ruby
handings = ['r', 'p', 's']
```

If you want get some value form the array you write:

```ruby
handings[0]
```

Gets first element form array. Elements are numbered form 0. You can change some
element:

```ruby
handings[1] = 'new value'
```

So we can generate computers move like this:

```ruby
handings = ['r', 'p', 's']
rand_num = rand(3)
computer = handings[rand_num]
puts "Computers move is #{computer}"
```

`#{}` interpolates its content as Ruby code and puts the result into the string.
This works only in double quoted strings.

But this prints one letter representation of computers move, we want nice
string. We need dictionary that translates one values to others - in Ruby such
dictionary is called **Hash**.

```ruby
names = { 'r' => 'rock', 'p' => 'paper', 's' => 'scissors' }
```

If you want translation of value 'r':

```ruby
names['r']
```

To change some translation:

```ruby
names['p'] = 'PAPER'
```

So, to print nice message with computers handing:

```ruby
puts "Computers move is #{names[computer]}"
```

Check who wins
--------------

Firs, check if there is a tie:

```ruby
if player == computer
  # it is a tie
  puts "It's a tie, try again!"
end
```

`==` checks if both expressions are equal, if they are it returns `true`,
otherwise it returns `false`. `if` checks if given expression is `true`, if it
is `true` code between `if` and `end` will be executed.

`# it is a tie` is a comment, comment is ignored by Ruby interpreter. For a
computer this line does not exist. It is information for programmer, you or
other team member.

When it is not a tie.

```ruby
if player == computer
  # it is a tie
  puts "It's a tie, try again!"
else
  # it is not a tie
end
```

We need another `Hash` that will tell as what handing beats what other handing.

```ruby
beats = { 'r' => 's', 'p' => 'r', 's' => 'p' }
```

and then we can check who beats who:

```ruby
if beats[player] == computer
  puts 'You won!'
else
  puts 'Computer won!'
end
```

What exactly `beats[player] == computer` do?
`player` is player handing, `computer` is computer handing. `beats` is a hash
that tells us what beats what. E.g. `'r' => 's'` - rock beats scissor.
So we can check what player can defeat. If players handing is 'r' then
`beats[player]` returns `s`, which means that player can defeat scissor. Then
we compare it to the computers hanging. If `beats[player]` matches `computer`
that means player defeats the computer. Otherwise, `else` section, computer
defeats player, we covered tie earlier.

Things like definitions of `handings`, `names` and `beats` should be at the
beginning of the program (after magic comment `#!...`)

You can checkout my solution. Current version is:

|version|
|:-----:|
| 1.0 |

> Read more about how to validate your project
  [validate project](/lessons/meta/validate_project.md)
