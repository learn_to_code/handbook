Ruby Basics 2
=============

Sample project for this chapter is https://gitlab.com/learn_to_code/blackjack

New game
--------

We will create game Blackjack. It is a card game. We will implement
simplified version of this game.

Rules
-----

* Player plays against the dealer.
* It is played with one deck of 52 cards.
* The objective is to beat the dealer in one of the following ways:
  * Get 21 points on the first two cards (it's called "blackjack").
  * Reach a final score higher than the dealer without exceeding 21.
  * Let the dealer draw additional cards until exceeds 21.
* Card points
  * Face cards (kings, queens and jacks) are counted as 10 points.
  * Ace is counted as 1 or 11 points, whatever is better.
  * Numeric cards are counted accordingly to their value.
* At the beginning, player gets two cards, dealer also gets two cards, but one
  card is face down (no one knows its value).
* player starts the game, player can:
  * hit - take another card.
  * stand - end his/her turn.
  * if player exceed 21 points it's a bust and dealer wins.
* after players turn there's a dealer turn.
  * dealer reveal the hidden card
  * dealer hits until gets 17 or more points
  * if dealer exceeds 21 points its bust and player wins.

We won't implement bets (so no splitting, doubling or surrendering)
And there is only one player.

Domain model
------------

* deck of cards: 52 cards
* card
  * suit: (spade: ♠, heard: ♥, club: ♣, diamond: ♦)
  * color: (red, black)
  * rank: (2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace)
  * points: Array of points, Card can have alternative points, like Ace
* hand - players or dealers cards
* dealer
* player

Visual design
-------------

```
========================================
dealer( ?): [ ?? ][ ♥ 3]
player(13): [ ♣ 3][ ♣ Q]
----------------------------------------
type: "h" to hit, "s" to stand -->
```

Scenario
--------

* deck shuffle
* draw two cards for player
* draw two cards for dealer, first one hidden
* player turn:
  1. if 'blackjack' end game with player victory
  2. read players action
  3. handle player action
    * if 'hit' draw a card for player
      * if bust, end game with dealer victory
      * if 18 points or more, end player turn
      * else goto 2.
    * if stand end player turn
* dealer turn:
  1. show hidden card
  2. if 'blackjack' end game with dealer victory
  3. if points over 16 end dealer turn
  4. draw a card for dealer
  5. if bust end game with player victory
  6. goto 3.
* if players score is higher than dealer then end game with victory of player
* else end game with victory of dealer.

Implementation requirements
---------------------------

* To print out use only `puts` and `print` methods.
* User must type in "h" to hit, "s" to stand.
* Final messages:
  * "It's a tie." for tie.
  * "Player lost!" if player lost the game.
  * "Player win!" if player win the game.
  * "Player BUST" if player bust (along with "Player lost!" message)
  * "Player BLACKJACK" if player win by blackjack (along with "Player win!"
    message)
  * "Dealer BUST!" / "Dealer BLACKJACK!" for bust and blackjack of dealer.

Game engine
-----------

Engine must play the scenario, after each step engine must display the table.
before game start, engine must prepare variables: deck, player hand, dealer hand
