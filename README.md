Learn to code
=============

Complete course of web development. For people without any development
experience.

This tutorial covers:

* front-end: HTML, CSS, bootstrap
* back-end: Ruby
* dev tools: Linux, git, atom, gitlab

Preparing
---------

How to prepare your computer to work -
[Workstation setup](lessons/meta/workstation_setup.md).

How to begin
------------

This tutorial is maintained in git repository. It is hosted on
https://gitlab.com - **https://gitlab.com/learn_to_code/handbook**.

You can just go to
[lessons/html_css/000_html_intro.md](lessons/html_css/000_html_intro.md)

**ToDo:** write some workflow description

> Gitlab is cool because it is available on open source license you can always
  go to your own git server or other service provider with same software.

*Fork* or *import* operation is part of your git web service, look for such
actions in the web page.

First lesson: *lessons/html_css/000_html_intro.md*.

> If you don't have any favorite text editor, I recommend you
  [Atom](http://atom.io). It's open source with great community. But any editor
  will do the job (e.g. sublime text, gedit, kate, vim). I don't recommend you
  any advanced IDE like Eclipse, Netbeans or RubyMine it will try to do many
  thinks for you but you should know what is going on here...

> Atom has **markdown-preview** plug-in, that lets you see side by side "rich"
  version of the document and source text.

Don't know git? Don't wary there is "git intro" in the first lesson.

License
-------

All materials in this course are distributed on Creative Commons
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license. License itself
is in file [LICENSE.txt](LICENSE.txt). You can find additional information at
https://creativecommons.org/licenses/by-sa/4.0/ where are translations to other
languages available.

### re-use of this material

If you are using this materials in your project don't forget to give an 
attribution:

"Learn to Code" by [Mariusz Kowalski](http://eniat.com) is licensed under
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

HTML version:

```html
"Learn to Code" by <a href="http://eniat.com">Mariusz Kowalski</a> is licensed
under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>
```

Text version:

```
"Learn to Code" by Mariusz Kowalski (http://eniat.com) is licensed under
CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
```

General information about Creative Commons licenses can be found at
https://creativecommons.org/licenses/

### Other licenses

If you require other form of licensing, like license use without "Attribution"
and/or "ShareAlike" clause please contact me via mairusz.kowalski@eniat.com

---

### Good luck and happy hacking!
